import { Injectable } from "@angular/core";
import { DataService } from "./data.service";
import { map } from "rxjs/operators"
import { Observable } from 'rxjs';

@Injectable()
export class PublicService {

    public urls = {
        imageUrl: 'http://116.203.178.137:8585/files/',
        cvUrl: 'http://116.203.178.137:8585/files/',
        videoUrl: 'http://116.203.178.137:8585/files/',
        voiceUrl: 'http://116.203.178.137:8585/files/'
    }

    constructor(public service: DataService) { }

    // get user info
    getUserInfo(): Observable<any> {
        let url = 'api/Account/GetUserInfo';
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // check token
    checkToken(): Observable<any> {
        let url = 'api/Account/CheckToken';
        return this.service.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get all provinces
    getProvince(): Observable<any> {
        let url = "Admin/api/Province";
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get cities by province id
    getCityByProvinceId(id): Observable<any> {
        let url = `Admin/api/City/GetCitiesByProvinceId/${id}`;
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get universities by city id
    getUniversitiesByCityId(id): Observable<any> {
        let url = `Admin/api/University/GetUniversitiesByCity/${id}`
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get university
    getUniversities(): Observable<any> {
        let url = "Admin/api/University";
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get education degree
    getEducationDegree(): Observable<any> {
        let url = "Admin/api/EducationDegree"
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get fields
    getFields(): Observable<any> {
        let url = "Admin/api/Field"
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get roles
    getRoles(): Observable<any> {
        let url = "api/Account/GetRoles"
        return this.service.get(url).pipe(map((response: any) => {
            return response
        }))
    }

}