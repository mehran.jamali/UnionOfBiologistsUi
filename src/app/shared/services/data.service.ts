import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})


export class DataService implements OnInit {
  // const baseUrl
  public baseUrl: any = {
    url: 'http://116.203.178.137:8585/',
  };

  private setHeaders(): any {
    let setHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let header = { headers: setHeader }
    return header;
  }

  constructor(private http: HttpClient,
    private router: Router) { }

  ngOnInit() {

  }
  get(url: string): Observable<Response> {
    return this.http.get(this.baseUrl.url + url, this.setHeaders())
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      );
  }
  post(url: string, data?: any, header?: any): Observable<Response> {
    return this.http.post(this.baseUrl.url + url, data, header ? header : this.setHeaders())
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      );
  }
  delete(url: string): Observable<Response> {
    return this.http.delete(this.baseUrl.url + url, this.setHeaders())
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      )
  }
  put(url: string, data?: any): Observable<Response> {
    return this.http.put(this.baseUrl.url + url, data, this.setHeaders())
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      );
  }

  // getDownload(id: number): Observable<any>{
  //   const reqHeader = new HttpHeaders ({
  //     'content-type': 'application/json',
  //     'Authorization': 'Bearer ' + localStorage.getItem('jazire')
  //  });
  //  return this.http.get(this.baseUrl.url + `Product/DownloadProductFile?productId=${id}`, { headers: reqHeader, responseType:'blob'});
  // }

  private handelError(error: any): any {
    return throwError(error || 'server Error');
  }
}