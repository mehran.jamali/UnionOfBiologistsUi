// Modules
import { NgModule, ModuleWithProviders } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// Services
import { DataService } from './services/data.service';
import { SharedService } from './shared.service'
import { PublicService } from "./services/public.service"

// Components
import { NavbarComponent } from "./components/navbar/navbar.component";
import { FooterComponent } from "./components/footer/footer.component";
import { SearchComponent } from "./components/search/search.component";
import { MemberItemComponent } from "./components/member-item/member-item.component";
import { EftekharatItemComponent } from "./components/eftekharat-item/eftekharat-item.component"
import { CommunityItemPostComponent } from "./components/community-item-post/community-item-post.component";
import { LoaderComponent } from "./components/loader/loader.component";

@NgModule({
    declarations: [
        NavbarComponent,
        FooterComponent,
        SearchComponent,
        MemberItemComponent,
        EftekharatItemComponent,
        CommunityItemPostComponent,
        LoaderComponent
    ],
    imports: [CommonModule, ReactiveFormsModule, FormsModule, RouterModule, FontAwesomeModule],
    exports: [
        // Modules
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        FontAwesomeModule,
        // Components
        NavbarComponent,
        FooterComponent,
        SearchComponent,
        MemberItemComponent,
        EftekharatItemComponent,
        CommunityItemPostComponent,
        LoaderComponent,
    ],
    bootstrap: []
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                DataService,
                SharedService,
                PublicService
            ]
        };
    }
}

