import { Injectable } from "@angular/core";
import { DataService } from "./services/data.service";
import { BehaviorSubject } from "rxjs"

@Injectable()
export class SharedService {
    // search data
    private searchSource = new BehaviorSubject('');
    public searchData = this.searchSource.asObservable();

    // previous url
    public previousUrl: string = '';

    // profile data
    public profileData: any;

    // union mode
    public unionMode: any = 1;

    constructor() { }

    // search
    setSearchFields(value: any) {
        this.searchSource.next(value);
    }

    getSearchFields() {
        return this.searchData;
    }

    // union
    setUnionMode(value: any) {
        this.unionMode = value;
    }

    getUnionMode() {
        return this.unionMode;
    }

    // profile
    setProfileData(data: any) {
        this.profileData = data
    }
    getProfileData() {
        return this.profileData
    }

    // previous url
    setPreviousUrl(value: string) {
        this.previousUrl = value;
    }
    getPreviousUrl() {
        return this.previousUrl
    }


}