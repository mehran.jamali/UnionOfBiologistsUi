import { Component, Input, SimpleChanges } from "@angular/core";
import { faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";

@Component({
    selector: 'footer-app',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})

export class FooterComponent {
    @Input() checkLogin: boolean;

    public isLogin: boolean;
    // icons
    faCaretLeft = faCaretLeft;

    constructor() { }

    ngOnInit(): void {
        AOS.init();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.isLogin = this.checkLogin;
    }

}