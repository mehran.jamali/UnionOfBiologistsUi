import { Component, Output } from "@angular/core";
import { faSearch, faCaretDown, faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { FormGroup, FormControl, Validators, FormGroupName } from "@angular/forms"
import * as AOS from "aos";
import { Router } from "@angular/router";

// Service
import { SharedService } from "../../shared.service";

@Component({
    selector: 'search-app',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})

export class SearchComponent {

    faSearch = faSearch;
    faCaretDown = faCaretDown;
    faAngleDown = faAngleDown;
    public categoryId = 1;
    public items: any = [
        { 'id': 1, 'name': 'مطالب' },
        { 'id': 2, 'name': 'رویداد' },
        { 'id': 3, 'name': 'انجمن' },
        { 'id': 4, 'name': 'استاد' },
    ]
    public searchData: any;
    public searchForm = new FormGroup({
        searchText: new FormControl("", [Validators.required])
    });

    constructor(
        public SharedService: SharedService,
        public router: Router,
    ) { }

    changeCategory(e) {
        // console.log(e.srcElement.value);
        this.categoryId = e.srcElement.value;
    }

    checkSearchTextValidation() {
        // console.log(this.searchForm.value.searchText);
    }

    search() {
        if (this.searchForm.value.searchText) {
            if (window.location.href == 'http://localhost:4200/search') {
                let data = { "categoryId": this.categoryId ? this.categoryId : "1", "searchText": this.searchForm.value.searchText };
                this.SharedService.setSearchFields(data);
            }
            else {
                let data = { "categoryId": this.categoryId ? this.categoryId : "1", "searchText": this.searchForm.value.searchText };
                this.SharedService.setSearchFields(data);
                this.router.navigate(['search']);
            }
        }
    }

    ngOnInit(): void {
        AOS.init();
        this.SharedService.searchData.subscribe(data => {
            this.searchData = data;
            this.searchForm.controls.searchText.setValue(this.searchData.searchText);
            // this.searchForm.controls.categoryId.setValue(this.searchData.categoryId);
        })
        if (this.searchData) {
            this.searchForm.value.searchText = this.searchData.searchText;
            this.categoryId = this.searchData.categoryId;
        }
    }
}