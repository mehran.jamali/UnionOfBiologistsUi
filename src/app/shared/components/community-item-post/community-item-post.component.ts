import { Component, Input, SimpleChanges } from "@angular/core";
import { faHeart, faComment, faCopy, faEye } from "@fortawesome/free-solid-svg-icons";
import { Router } from "@angular/router";
import * as $ from "jquery";
import * as AOS from "aos";
import { PublicService } from "../../../shared/services/public.service";

@Component({
    selector: 'community-item-post-app',
    templateUrl: './community-item-post.component.html',
    styleUrls: ['./community-item-post.component.scss']
})

export class CommunityItemPostComponent {

    // data
    @Input() data: any;

    // image
    public imageUrl = this.publicService.urls.imageUrl;
    public defaultImageUrl = "DefaultImages/article.jpg";

    // icons
    faHeart = faHeart;
    faComment = faComment;
    faCopy = faCopy;
    faEye = faEye;

    constructor(public router: Router, public publicService: PublicService) { }
    // showPostType(value: any) {
    //     if (value == 1) {
    //         return "مقاله"
    //     }
    //     else if (value == 2) {
    //         return "رویداد"
    //     }
    //     else if (value == 3) {
    //         return "اخبار"
    //     }
    //     else if (value == 4) {
    //         return "عکس"
    //     }
    //     else if (value == 5) {
    //         return "ویدیو"
    //     }
    //     else if (value == 6) {
    //         return "پادکست"
    //     }
    //     else if (value == 7) {
    //         return "اطلاعیه"
    //     }
    //     else {
    //         return "error"
    //     }
    // }

    changeUrl() {
        let url = '';
        if (this.data.type == 7) {
            url = `/notification/${this.data.id}`
        }
        else {
            url = `/article/${this.data.id}`
        }
        this.router.navigate([url])
    }

    copyLink() {
        document.addEventListener('copy', (e: ClipboardEvent) => {
            let url = "localhost:4200" + this.router.url;
            e.clipboardData.setData('text/plain', url);
            e.preventDefault();
        });
        document.execCommand('copy');
    }

    ngOnInit(): void {

        AOS.init();
        $(document).ready(function () {
            $('.tooltip').click(function () {
                $('.tooltip').removeClass('switch-tooltip');
                $(this).addClass('switch-tooltip');
                setTimeout(function () {
                    $('.tooltip').removeClass('switch-tooltip');
                }, 1000)

            })
        })
    }

}
