import { Component } from "@angular/core";
import * as AOS from "aos";

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
    constructor() { }
    ngOnInit(): void {
        AOS.init()
    }
}