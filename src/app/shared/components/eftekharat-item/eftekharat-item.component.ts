import { Component, Input } from "@angular/core";
import * as AOS from "aos";

@Component({
    selector: 'eftekharat-item-app',
    templateUrl: './eftekharat-item.component.html',
    styleUrls: ['./eftekharat-item.component.scss']
})

export class EftekharatItemComponent {
    @Input() data: any;

    constructor() { }
    ngOnInit(): void {
        AOS.init()
    }
}