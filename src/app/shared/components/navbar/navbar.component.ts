import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import * as $ from "jquery";
import * as AOS from "aos";
import { SharedService } from "../../shared.service";
import { PublicService } from "../../services/public.service";
import { Router } from "@angular/router";
import { faBars } from "@fortawesome/free-solid-svg-icons"

@Component({
    selector: 'navbar-app',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {

    // images
    public imageUrl = this.publicService.urls.imageUrl
    public defaultImageUrl = 'defaultImages/user.jpg'

    // get data from parent
    @Input() default: boolean;
    @Input() checkLogin: boolean;
    // @Input() checkShowProfile: boolean;

    // Token
    public token = localStorage.getItem('token');

    // Icons
    public faBars = faBars;

    // Show Menu
    public showMenuV = 0;

    // Check Navbar mode
    public navbarMode: any;

    // Check Login Variable
    public isLogin: boolean;

    // Show Profile
    public showProfile: boolean;

    // Profile data
    public profileData: any;

    // Login data
    public loginId = ''
    public loginRole = ''

    constructor(public sharedService: SharedService, public publicService: PublicService, public router: Router) { }

    logout() {
        localStorage.setItem("token", '');
        localStorage.setItem("role", '');
        localStorage.setItem("id", '');
        this.isLogin = false;
    }
    // get user data
    getUserInfo() {
        this.publicService.getUserInfo().subscribe(data => {
            this.profileData = data;
            console.log(this.profileData);
        }, error => {
            console.log(error);
        })
    }

    // set previous url
    setPreviousUrl() {
        let url = this.router.url;
        this.sharedService.setPreviousUrl(url);
    }

    // go profile 
    goProfile() {
        if (this.loginRole == 'User') {
            let url = '/user-profile/' + this.loginId;
            this.router.navigate([url])
        }
        else if (this.loginRole == 'Professor') {
            let url = '/professor/' + this.loginId;
            this.router.navigate([url])
        }
        else if (this.loginRole == 'StartUp') {
            let url = '/startup/' + this.loginId;
            this.router.navigate([url])
        }
        else if (this.loginRole == 'Union') {
            let url = '/union/' + this.loginId;
            this.router.navigate([url])
        }

    }

    // go panel
    goPanel() {
        let url = `http://116.203.178.137:4444?token=${localStorage.getItem('token')}`;
        location.replace(url);
    }

    // Show Menu
    showMenu() {
        if (!this.showMenuV) {
            this.showMenuV = 1;
            document.getElementById('nav').classList.remove('inline-menu');
        }
        else {
            this.showMenuV = 0;
            document.getElementById('nav').classList.add('inline-menu');
        }
    }

    ngOnInit(): void {

        let data = this.sharedService.getProfileData()

        this.loginId = localStorage.getItem("id");
        this.loginRole = localStorage.getItem("role");

        this.profileData = data ? data : {
            "fullName": "",
            "profileImage": "",
            "coverImage": ""
        }

        console.log(this.profileData);

        if (window.performance.navigation.type == 1) {
            this.getUserInfo()
        }

        AOS.init();
        this.default ? document.getElementById('navbarMode').classList.add("default")
            : document.getElementById('navbarMode').classList.add("non-default")

        $(document).ready(function () {
            if ($(window).scrollTop() > 50) {
                $('#navbar').removeClass('navbar-padding');
                $('#navbar').addClass('scrolling-navbar-padding');
                $('#profile-box').addClass('scrolling-profile-box');
            }
            else {
                $('#navbar').removeClass('scrolling-navbar-padding');
                $('#navbar').addClass('navbar-padding');
                $('#profile-box').removeClass('scrolling-profile-box');
            }
            $(window).scroll(function () {
                if ($(document).scrollTop() > 50) {
                    $('#navbar').removeClass('navbar-padding');
                    $('#navbar').addClass('scrolling-navbar-padding');
                    $('#profile-box').addClass('scrolling-profile-box');
                }
                else {
                    $('#navbar').removeClass('scrolling-navbar-padding');
                    $('#navbar').addClass('navbar-padding');
                    $('#profile-box').removeClass('scrolling-profile-box');
                }
            })

            $('#profile-image').hover(function () {
                $("#profile-box").toggleClass('show-profile-box')
            })
        })
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.isLogin = this.checkLogin;
        // this.showProfile = this.checkShowProfile;

        this.default ? document.getElementById('navbarMode').classList.add("default")
            : document.getElementById('navbarMode').classList.add("non-default")
    }

}
