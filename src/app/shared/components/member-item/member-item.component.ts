import { Component, OnInit, Input } from '@angular/core';
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import { PublicService } from "../../services/public.service";

@Component({
  selector: 'app-member-item',
  templateUrl: './member-item.component.html',
  styleUrls: ['./member-item.component.scss']
})
export class MemberItemComponent implements OnInit {

  // data
  @Input() data: any;

  // image
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImageUrl = 'DefaultImages/user.jpg';

  // icons 
  faAngleLeft = faAngleLeft;

  constructor(public publicService: PublicService) { }

  ngOnInit() {
    AOS.init();
  }

}
