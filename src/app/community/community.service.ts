import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';


import { DataService } from "../shared/services/data.service";
import { PublicService } from "../shared/services/public.service";

@Injectable()
export class CommunityService {
    constructor(public dataService: DataService, public publicService: PublicService) { }

    // get union list
    getUnionList(data): Observable<any> {
        let url = "api/List/GetUnions";
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get union detail
    getUnionDetail(id): Observable<any> {
        let url = `api/UnionProfile/GetUnionInfo/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // union profile is followed
    unionIsFollowed(id): Observable<any> {
        let url = `api/UnionProfile/IsFollowed/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // follow
    follow(id): Observable<any> {
        let url = `api/Follow/FollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // unfollow
    unFollow(id): Observable<any> {
        let url = `api/Follow/UnFollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get article
    getArticles(data): Observable<any> {
        let url = 'api/UnionProfile/GetArticles';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get notification
    getNotifications(data): Observable<any> {
        let url = "api/UnionProfile/GetNotifications";
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get members
    getMembers(data): Observable<any> {
        let url = 'api/UnionProfile/GetUnionMembers';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get journals
    getJournals(data): Observable<any> {
        let url = "api/UnionProfile/GetJournals";
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get honors
    getHonors(data): Observable<any> {
        let url = 'api/UnionProfile/GetHonors';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get aboutUs
    getAboutUs(id): Observable<any> {
        let url = `api/UnionProfile/GetUnionAboutUsWeb/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // get contactUs
    getContactUs(id): Observable<any> {
        let url = `api/UnionProfile/GetContactUs/${id}`;
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }
}