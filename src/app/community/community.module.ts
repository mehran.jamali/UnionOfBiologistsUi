// Modules
import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";

// Components
import { CommunityComponent } from "./community.component";
import { CommunityItemComponent } from "./community-item/community-item.component";
import { CommunityDetailComponent } from "./community-detail/community-detail.component";
import { DetailHomeComponent } from "./community-detail/detail-home/detail-home.component";
import { DetailNashriatComponent } from './community-detail/detail-nashriat/detail-nashriat.component';
import { DetailMemberComponent } from './community-detail/detail-member/detail-member.component';
import { DetailEftekharatComponent } from './community-detail/detail-eftekharat/detail-eftekharat.component';
import { DetailAboutComponent } from './community-detail/detail-about/detail-about.component';
import { DetailContactUsComponent } from './community-detail/detail-contact-us/detail-contact-us.component';
import { HomeAllpostComponent } from './community-detail/detail-home/home-allpost/home-allpost.component';
import { HomeArticleComponent } from './community-detail/detail-home/home-article/home-article.component';
import { HomeEventComponent } from './community-detail/detail-home/home-event/home-event.component';
import { HomeNewsComponent } from './community-detail/detail-home/home-news/home-news.component';
import { HomeNotificationComponent } from './community-detail/detail-home/home-notification/home-notification.component';
import { NashriatItemComponent } from "./community-detail/detail-nashriat/nashriat-item/nashriat-item.component";
import { HomeImageComponent } from './community-detail/detail-home/home-image/home-image.component';
import { HomePodcastComponent } from './community-detail/detail-home/home-podcast/home-podcast.component';
import { HomeVideoComponent } from './community-detail/detail-home/home-video/home-video.component';

// Services
import { CommunityService } from "./community.service";


@NgModule({
    declarations: [
        CommunityComponent,
        CommunityItemComponent,
        CommunityDetailComponent,
        DetailHomeComponent,
        DetailNashriatComponent,
        DetailMemberComponent,
        DetailEftekharatComponent,
        DetailAboutComponent,
        DetailContactUsComponent,
        HomeAllpostComponent,
        HomeArticleComponent,
        HomeEventComponent,
        HomeNewsComponent,
        HomeNotificationComponent,
        NashriatItemComponent,
        HomeImageComponent,
        HomePodcastComponent,
        HomeVideoComponent,
    ],
    imports: [
        SharedModule.forRoot(),
        ReactiveFormsModule
    ],
    providers: [
        CommunityService
    ]
})
export class CommunityModule {
    constructor() { }
}