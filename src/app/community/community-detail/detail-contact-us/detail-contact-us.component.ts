import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import { faPhone, faMailBulk, faLocationArrow, faAddressCard } from "@fortawesome/free-solid-svg-icons"
import { CommunityService } from "../../community.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-detail-contact-us',
  templateUrl: './detail-contact-us.component.html',
  styleUrls: ['./detail-contact-us.component.scss']
})
export class DetailContactUsComponent implements OnInit {

  // icons
  faPhone = faPhone;
  faMailBulk = faMailBulk;
  faLocationArrow = faLocationArrow;
  faSearchLocation = faAddressCard;

  // loader
  public showContentLoader = false;

  // data
  public data: any = {
    'phoneNumber': '',
    'email': '',
    'address': '',
    'telegram': ''
  };
  public unionId: any = '';

  constructor(public communityService: CommunityService, public activatedRoute: ActivatedRoute) { }

  getContactUs() {
    this.communityService.getContactUs(this.unionId).subscribe(data => {
      this.data = data
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    })
  }

  ngOnInit() {
    this.unionId = this.activatedRoute.snapshot.params['id'];
    this.getContactUs()
    AOS.init()
    console.clear()
  }

  ngOnDestroy(): void {
    console.clear()
  }

}
