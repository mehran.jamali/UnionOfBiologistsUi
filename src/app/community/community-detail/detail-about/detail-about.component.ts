import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import { CommunityService } from "../../community.service";
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-detail-about',
  templateUrl: './detail-about.component.html',
  styleUrls: ['./detail-about.component.scss']
})
export class DetailAboutComponent implements OnInit {

  // loader
  public showContentLoader = false;

  // data
  public data: any = {
    'aboutUnion': ''
  };
  public unionId: any = '';

  constructor(public communityService: CommunityService, public activatedRoute: ActivatedRoute) { }

  getAboutText() {
    this.communityService.getAboutUs(this.unionId).subscribe(data => {
      this.data = data
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    })
  }

  ngOnInit() {
    this.unionId = this.activatedRoute.snapshot.params['id'];
    this.getAboutText()
    AOS.init()
  }

}
