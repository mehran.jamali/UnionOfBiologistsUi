import { Component, Input, SimpleChanges } from "@angular/core";
import * as AOS from "aos";
import * as $ from "jquery";

@Component({
    selector: 'detail-home-app',
    templateUrl: './detail-home.component.html',
    styleUrls: ['./detail-home.component.scss']
})

export class DetailHomeComponent {

    // check farhangi honari
    @Input() unionMode: any;
    public isFarhangiHonari: boolean = false;

    // title
    public contentTitle: string = "همه پست ها";
    public tabName: string = 'allPost';

    // show loader
    public showContentLoader = false;


    changeHomeTabs(value: string) {
        this.showContentLoader = true;
        setTimeout(() => {
            this.showContentLoader = false;
        }, 500);
        this.tabName = value;
        if (value == 'allPost') {
            this.contentTitle = "همه پست ها"
        }
        else if (value == 'article') {
            this.contentTitle = "مقالات"
        }
        else if (value == 'event') {
            this.contentTitle = "رویداد"
        }
        else if (value == 'news') {
            this.contentTitle = "اخبار"
        }
        else if (value == 'image') {
            this.contentTitle = "عکس"
        }
        else if (value == 'video') {
            this.contentTitle = "ویدیو"
        }
        else if (value == "podcast") {
            this.contentTitle = "پادکست"
        }
        else if (value == 'notification') {
            this.contentTitle = 'اطلاعیه'
        }
    }
    constructor() { }

    ngOnInit(): void {
        if (this.unionMode == 3) {
            this.isFarhangiHonari = true;
        }
        else {
            this.isFarhangiHonari = false;
        }

        AOS.init()
        $(document).ready(function () {
            $('.nav-tab').click(function () {
                $('.nav-tab').removeClass('nav-active');
                $(this).addClass('nav-active');

                $('.nav-content').removeClass('nav-content-active');
                $('#' + $(this).attr('role')).addClass('nav-content-active');
            })
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        // do nothing
    }

}