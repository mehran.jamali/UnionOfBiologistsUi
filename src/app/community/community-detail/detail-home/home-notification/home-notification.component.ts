import { Component, OnInit } from '@angular/core';

import { CommunityService } from "../../../community.service";
import { ActivatedRoute } from "@angular/router";
import * as $ from "jquery";


@Component({
  selector: 'app-home-notification',
  templateUrl: './home-notification.component.html',
  styleUrls: ['./home-notification.component.scss']
})
export class HomeNotificationComponent implements OnInit {
  // loader
  public showContentLoader = false;

  // union id
  public unionId = ''

  // page
  public pageNumber = 0;
  public pageSize = 0;

  // list
  public items: any = []

  constructor(public communityService: CommunityService, public activatedRoute: ActivatedRoute) { }

  getNotifications() {
    this.pageNumber += 1;
    this.pageSize = 3;
    let data = {
      "unionId": this.unionId,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }
    this.communityService.getNotifications(data).subscribe(data => {
      this.items.push(...data);
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);

    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    this.unionId = this.activatedRoute.snapshot.params['id'];
    this.getNotifications()
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }


  scroll = (event): void => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getNotifications()
    }
  };

}

