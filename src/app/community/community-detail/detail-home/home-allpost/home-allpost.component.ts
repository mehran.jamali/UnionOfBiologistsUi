import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import { CommunityService } from "../../../community.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-home-allpost',
  templateUrl: './home-allpost.component.html',
  styleUrls: ['./home-allpost.component.scss']
})
export class HomeAllpostComponent implements OnInit {

  // loader
  public showContentLoader = false;

  // union id
  public unionId = ''

  // page
  public pageNumber = 0;
  public pageSize = 0;

  // list
  public items: any = []

  constructor(public communityService: CommunityService, public activatedRoute: ActivatedRoute) { }

  getArticles() {
    this.pageNumber += 1;
    this.pageSize = 3;
    let data = {
      "articleType": 1,
      "unionId": this.unionId,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }
    this.communityService.getArticles(data).subscribe(data => {
      this.items.push(...data);
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);

    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    this.unionId = this.activatedRoute.snapshot.params['id'];
    this.getArticles()
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }


  scroll = (event): void => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getArticles()
    }
  };

}
