import { Component } from "@angular/core";
import * as AOS from "aos";
import * as $ from "jquery";
import { ActivatedRoute } from "@angular/router"

// icons
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

// Service
import { SharedService } from "../../shared/shared.service";
import { CommunityService } from "../community.service";
import { PublicService } from "../../shared/services/public.service";

@Component({
    selector: 'community-detail-app',
    templateUrl: './community-detail.component.html',
    styleUrls: ['./community-detail.component.scss']
})
export class CommunityDetailComponent {

    // data for child components
    public isLogin: boolean = false;
    public navbarMode: boolean = true;

    // union
    public unionMode: number = 0;
    public unionId: string;

    // user id
    public userId: string = '';

    // tab
    public tabName: string = '';

    // images
    public imageUrl = this.publicService.urls.imageUrl
    public defaultCoverImageUrl = 'DefaultImages/unionCover.jpg'
    public defaultProfileImageUrl = 'DefaultImages/unionProfile.jpg'

    // data
    public unionDetail: any = {
        "name": "",
        "profileImage": "",
        "coverImage": "",
        "slogan": "",
        "unionType": 0
    };

    // list
    public all = {}

    // follow
    public isFollowed: boolean = false;

    // show loader
    public showLoader: boolean = true;
    public showContentLoader: boolean = false;

    // icon
    faAngleLeft = faAngleLeft

    constructor(public sharedService: SharedService, public communityService: CommunityService, public activatedRoute: ActivatedRoute, public publicService: PublicService) { }

    // change side tab
    changeSideTabs(value: string) {
        this.tabName = value;
        this.showContentLoader = true;
        setTimeout(() => {
            this.showContentLoader = false;
        }, 500);
    }

    // follow
    follow() {
        this.communityService.follow(this.unionId).subscribe(data => {
            this.isFollowed = true
        }, error => { })
    }

    // un follow
    unFollow() {
        this.communityService.unFollow(this.unionId).subscribe(data => {
            this.isFollowed = false
        }, error => { })
    }

    // get union detail 
    getUnionDetail() {
        this.communityService.getUnionDetail(this.unionId).subscribe(data => {
            this.unionDetail = data;
            this.unionMode = this.unionDetail.unionType;
            setTimeout(() => {
                this.showLoader = false;
                this.showContentLoader = true;
                this.tabName = 'home';
                setTimeout(() => {
                    this.showContentLoader = false;
                }, 500);
            }, 1000);
        }, error => { })
    }

    // check token
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.isLogin = true
        }, error => {
            this.isLogin = false
        })
    }

    // check follow
    checkFollow() {
        this.communityService.unionIsFollowed(this.unionId).subscribe(data => {
            this.isFollowed = data;
        })
    }

    ngOnInit(): void {

        this.userId = localStorage.getItem('id')

        this.checkToken()

        this.unionId = this.activatedRoute.snapshot.params['id'];
        if (this.unionId) {
            this.checkFollow()
            this.getUnionDetail()
        }

        AOS.init();
        $(document).ready(function () {
            // this.contentTitle = "همه پست ها"
            $('.tab').click(function () {
                $('.tab').removeClass('active');
                $(this).addClass('active');

                $('.content').removeClass('content-active');
                $('#' + $(this).attr('role')).addClass('content-active');
            })
        });
    }
}