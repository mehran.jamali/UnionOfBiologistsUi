import { Component, OnInit, Input } from "@angular/core";
import { faDownload } from "@fortawesome/free-solid-svg-icons"
import * as AOS from "aos";
import { PublicService } from "../../../../shared/services/public.service";

@Component({
    selector: 'nashriat-item-app',
    templateUrl: './nashriat-item.component.html',
    styleUrls: ['./nashriat-item.component.scss']
})

export class NashriatItemComponent {

    // data
    @Input() data: any;

    // image
    public imageUrl = this.publicService.urls.imageUrl;
    public defaultImageUrl = 'DefaultImages/article.jpg';


    // icons 
    faDownload = faDownload;

    constructor(public publicService: PublicService) { }
    ngOnInit(): void {
        AOS.init();
    }
}