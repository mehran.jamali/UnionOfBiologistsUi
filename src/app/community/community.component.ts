import { Component } from "@angular/core";
import * as $ from "jquery";
import * as AOS from "aos";

// icons
import { faAngleRight, faAngleLeft, faSearch } from "@fortawesome/free-solid-svg-icons";

// forms
import { FormGroup, FormControl } from "@angular/forms"

// Service
import { PublicService } from "../shared/services/public.service";
import { CommunityService } from "./community.service";
import { SharedService } from "../shared/shared.service";

@Component({
    selector: 'community-app',
    templateUrl: './community.component.html',
    styleUrls: ['./community.component.scss']
})

export class CommunityComponent {
    // navbar data
    public checkLogin: boolean = false;
    public navbarMode: boolean = false;

    // page controller
    public pageNumber = 1;
    public pageSize = 4;
    setDefaultPage() {
        this.pageNumber = 1;
        this.pageSize = 4;
        console.log("page number: ", this.pageNumber, " page size: ", this.pageSize);
    }

    // show loader
    public showLoader = true;
    public showListLoader = false;

    // selected values
    public selectValues =
        {
            "unionType": 0,
            "cityId": 0,
            "universityId": 0,
            "fieldId": 0,
        }

    // union type
    public unionMode = 1;

    // icons
    faAngleRight = faAngleRight;
    faAngleLeft = faAngleLeft;
    faSearch = faSearch;


    // form
    public filterForm = new FormGroup({
        search: new FormControl("", []),
        province: new FormControl("", []),
        city: new FormControl({ value: '', disabled: true }, []),
        university: new FormControl({ value: '', disabled: true }, []),
        field: new FormControl("", []),
    })


    // lists
    public unionList = []
    public provinces = [];
    public cities = []
    public universities = []
    public fields = []

    public filteredProvinces;
    public filteredCities;
    public filteredUniversities;
    public filteredFields;

    // select 
    // select province 
    setProvinceValue(value) {
        this.filterForm.controls.province.setValue(value.name);
        this.filterForm.controls['city'].enable();
        this.filteredProvinces = this.provinces;
        this.getCityByProvinceId(value.id);
        this.setDefaultPage()
    }

    // select city
    setCityValue(value) {
        this.filterForm.controls.city.setValue(value.name);
        this.filterForm.controls['university'].enable();
        this.selectValues.cityId = value.id;
        this.filteredCities = this.cities;
        this.getUniversityByCityId(value.id)
        console.log(this.selectValues);
        this.setDefaultPage()
    }

    // select university
    setUniversityValue(value) {
        this.filterForm.controls.university.setValue(value.name);
        this.selectValues.universityId = value.id;
        this.filteredUniversities = this.universities;
        console.log(this.selectValues);
        this.setDefaultPage()
    }

    // select field
    setFieldValue(value) {
        this.filterForm.controls.field.setValue(value.name);
        this.selectValues.fieldId = value.id;
        this.filteredFields = this.fields;
        console.log(this.selectValues);
        this.setDefaultPage()
    }

    // filter 
    // filter province
    filterProvince() {
        this.filteredProvinces = this.provinces.filter(x => {
            return x.name.includes(this.filterForm.value.province);
        })
    }

    // filter city
    filterCity() {
        this.filteredCities = this.cities.filter(x => {
            return x.name.includes(this.filterForm.value.city);
        })
    }

    // filter university
    filterUniversity() {
        this.filteredUniversities = this.universities.filter(x => {
            return x.name.includes(this.filterForm.value.university);
        })
    }

    // filter field
    filterField() {
        this.filteredFields = this.fields.filter(x => {
            return x.name.includes(this.filterForm.value.field);
        })
    }


    // constructor
    constructor(public sharedService: SharedService, public publicService: PublicService, public communityService: CommunityService) { }

    // change tab
    changeTab(number: any) {
        this.unionMode = number;
        this.sharedService.setUnionMode(number);
        console.log("tab: ", number);
        this.setDefaultPage();
        this.doFilter();
    }

    // go previous
    goPrevious() {
        if (this.pageNumber > 1) {
            this.pageNumber -= 1;
            this.pageSize = 4;
            this.doFilter()
        }
        else {
            // do nothing
        }
    }

    // go next
    goNext() {
        if (this.unionList.length < 4) {
            if (this.unionList.length == 0) {
                this.pageNumber -= 1;
                this.pageSize = 4;
                this.doFilter()
            }
        }
        else {
            this.pageNumber += 1;
            this.pageSize = 4;
            this.doFilter()
        }
    }

    // get province
    getProvince() {
        this.publicService.getProvince().subscribe(data => {
            this.provinces = data;
            this.filteredProvinces = this.provinces;
        }, error => {
            // do nothing
        })
    }

    // get city by province id
    getCityByProvinceId(id) {
        this.publicService.getCityByProvinceId(id).subscribe(data => {
            this.cities = data;
            this.filteredCities = this.cities;
        }, error => {
            // do nothing

        })
    }

    // get university by city id
    getUniversityByCityId(id) {
        this.publicService.getUniversitiesByCityId(id).subscribe(data => {
            this.universities = data;
            this.filteredUniversities = this.universities;
        }, error => {
            // do nothing

        })
    }

    // get fields 
    getFields() {
        this.publicService.getFields().subscribe(data => {
            this.fields = data;
            this.filteredFields = this.fields
        }, error => {
            // do nothing
        })
    }

    // do filter 
    doFilter() {
        let body = {
            "unionType": this.unionMode,
            "cityId": this.selectValues.cityId,
            "universityId": this.selectValues.universityId,
            "fieldId": this.selectValues.fieldId,
            "searchText": this.filterForm.value.search != "" ? this.filterForm.value.search : "",
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber
        }
        this.unionList = [];
        this.showListLoader = true;

        this.communityService.getUnionList(body).subscribe(data => {
            this.unionList = data;
            if (this.showLoader) {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 1500);
            }
            else {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 500);
            }
            setTimeout(() => {
                this.showLoader = false;
            }, 1000);

        }, error => {
            setTimeout(() => {
                this.showListLoader = false;
            }, 500);
        })
    }

    // check token
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
    }

    ngOnInit(): void {

        this.checkToken();

        this.doFilter();

        this.getProvince();
        this.getFields()

        AOS.init();

        $(document).ready(function () {
            $("#tab-box div").click(function () {
                $("#tab-box div").removeClass('active');
                $(this).addClass("active");
            })
            // open dropdown
            $(".select-input").focus(function () {
                $("#" + $(this).attr('name')).addClass("show-dropdown");
            })
            // close dropdown
            $(".select-input").focusout(function () {
                $("#" + $(this).attr('name')).removeClass("show-dropdown");
            })
        })
        // this.sharedService.setUnionMode(this.unionMode);
    }
}