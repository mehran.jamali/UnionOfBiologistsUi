import { Component, Input } from "@angular/core";
import { PublicService } from "../../shared/services/public.service";
import * as AOS from "aos";

@Component({
    selector: 'community-item-app',
    templateUrl: './community-item.component.html',
    styleUrls: ['./community-item.component.scss']
})

export class CommunityItemComponent {
    // data
    @Input() data: any;

    // images
    public imageUrl = this.publicService.urls.imageUrl
    public defaultImageUrl = 'DefaultImages/union.jpg'


    constructor(public publicService: PublicService) { }
    ngOnInit(): void {
        AOS.init();
    }
}