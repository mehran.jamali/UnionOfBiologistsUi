import { Component } from "@angular/core";
import * as AOS from "aos";
import { faPhone, faMailBulk, faLocationArrow, faAddressCard } from "@fortawesome/free-solid-svg-icons"
import { PublicService } from "../shared/services/public.service";

@Component({
    selector: 'app-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.component.scss']
})

export class ContactUsComponent {

    public checkLogin: boolean = false;
    public navbarMode: boolean = false;

    // loader 
    public showLoader: boolean = true;

    public data = {
        "phone": '09123456789',
        'email': 'example.email@gmail.com',
        'telegram': '@zist_shenasan',
        'location': 'اردبیل - اردبیل -میدان شریعتی ، ساختمان الماس شهر ، طبقه هفتم'
    }

    // icons
    faPhone = faPhone;
    faMailBulk = faMailBulk;
    faLocationArrow = faLocationArrow;
    faSearchLocation = faAddressCard;

    constructor(public publicService: PublicService) { }

    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
        setTimeout(() => {
            this.showLoader = false;
        }, 1000);
    }

    ngOnInit() {
        this.checkToken();
        AOS.init()
    }
}
