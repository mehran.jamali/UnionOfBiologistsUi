import { NgModule } from '@angular/core';
import { ContactUsComponent } from "./contact-us.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    SharedModule.forRoot()
  ]
})
export class ContactUsModule { }
