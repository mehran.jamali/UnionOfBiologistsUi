import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";
import { PublicService } from "../../shared/services/public.service";

@Component({
  selector: 'app-dashboard-article',
  templateUrl: './dashboard-article.component.html',
  styleUrls: ['./dashboard-article.component.scss']
})
export class DashboardArticleComponent implements OnInit {

  @Input() data: any;

  // image 
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImage = ''

  constructor(public publicService: PublicService) { }

  ngOnInit() {
    AOS.init()
  }

}
