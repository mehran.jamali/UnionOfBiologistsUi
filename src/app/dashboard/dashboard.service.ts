import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';


import { DataService } from "../shared/services/data.service";
import { PublicService } from "../shared/services/public.service";
import { resolve } from 'url';

@Injectable()
export class DashboardService {
    constructor(public dataService: DataService, public publicService: PublicService) { }

    // Get Followed Unions
    getFollowedUnions(data: any): Observable<any> {
        let url = 'api/Feed/GetFollowedUnions';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // Get Followed Professor
    getFollowedProfessors(data: any): Observable<any> {
        let url = 'api/Feed/GetFollowedProfessors';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // Get Followed StartUps
    getFollowedStartUps(data: any): Observable<any> {
        let url = 'api/Feed/GetFollowedStartUps';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // Get Suggested Unions
    getSuggestedUnions(): Observable<any> {
        let url = "api/Feed/GetSuggestedUnions/3";
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }
    // Get Suggested Professors
    getSuggestedProfessors(): Observable<any> {
        let url = "api/Feed/GetSuggestedProfessors/3";
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }
    // Get Suggested StartUps
    getSuggestedStartUps(): Observable<any> {
        let url = "api/Feed/GetSuggestedStartUps/3";
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // Get Event List
    getEventList(): Observable<any> {
        let url = "api/Feed/GetEventList/2";
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // Get Most Viewed Articles
    getMostViewedArticles(): Observable<any> {
        let url = "api/Feed/GetMostViewedArticles/3";
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // Get Articles
    getArticles(data: any): Observable<any> {
        let url = "api/Feed/GetArticles";
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // Follow User
    followUser(id: any): Observable<any> {
        let url = `api/Follow/FollowUser/${id}`
        return this.dataService.post(url).pipe(map((response) => {
            return response
        }))
    }

    // Follow User
    unFollowUser(id: any): Observable<any> {
        let url = `api/Follow/UnFollowUser/${id}`
        return this.dataService.post(url).pipe(map((response) => {
            return response
        }))
    }

}