// Modules 
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";

// Component
import { DashboardComponent } from "./dashboard.component";
import { DashboardArticleComponent } from './dashboard-article/dashboard-article.component';

// Service
import { DashboardService } from "./dashboard.service";

@NgModule({
  declarations: [DashboardComponent, DashboardArticleComponent],
  imports: [
    SharedModule.forRoot()
  ],
  providers: [
    DashboardService
  ]
})
export class DashboardModule { }
