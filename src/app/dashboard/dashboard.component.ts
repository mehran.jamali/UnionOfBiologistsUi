import { Component } from "@angular/core";
import { faAngleLeft, faPlus } from "@fortawesome/free-solid-svg-icons";
import * as $ from "jquery";
import * as AOS from "aos";

import { Router } from "@angular/router";
import { DashboardService } from "./dashboard.service";
import { PublicService } from "../shared/services/public.service";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {
    // check
    public isLogin: boolean = true;
    public navbarMode: boolean = true;

    // icons
    faAngleLeft = faAngleLeft;
    faPlus = faPlus;

    // loader 
    public showLoader: boolean = true;
    public showListLoader: boolean = true;

    // image
    public ImageUrl = this.publicService.urls.imageUrl;
    public defaultUserImage = '';
    public defaultEventImage = '';
    public defaultArticleImage = '';
    public defaultUserCoverImage = '';

    // user data
    public userData = {
        "fullName": "string",
        "profileImage": "string",
        "coverImage": "string"
    };

    // page
    public pageNumber = 0;
    public pageSize = 0;

    // union
    public unionPageNumber = 0;
    public unionPageSize = 0;

    // startUp
    public startUpPageNumber = 0;
    public startUpPageSize = 0;

    // professor
    public professorPageNumber = 0;
    public professorPageSize = 0;


    constructor(public dashboardService: DashboardService, public publicService: PublicService, public router: Router) { }

    // list
    public unionFollowedList = []

    public professorFollowedList = []

    public startupFollowedList = []

    public professorSuggestedList = []

    public unionSuggestedList = []

    public startupSuggestedList = []

    public eventProgressiveList = []

    public articleMostVisited = []

    public articleMainList = []


    // Get Followed Unions
    getFollowedUnions() {
        let data = {
            "pageSize": this.unionPageSize = 6,
            "pageNumber": this.unionPageNumber += 1
        }
        this.dashboardService.getFollowedUnions(data).subscribe(data => {
            this.unionFollowedList = [...data];
        }, error => {
            console.clear()
        })
    }

    // Get Followed Professors
    getFollowedProfessors() {
        let data = {
            "pageSize": this.professorPageSize = 6,
            "pageNumber": this.professorPageNumber += 1
        }
        this.dashboardService.getFollowedProfessors(data).subscribe(data => {
            this.professorFollowedList = [...data];
        }, error => {
            console.clear()
        })
    }
    // Get Followed StartUps
    getFollowedStartUps() {
        let data = {
            "pageSize": this.startUpPageSize = 6,
            "pageNumber": this.startUpPageNumber += 1
        }
        this.dashboardService.getFollowedStartUps(data).subscribe(data => {
            this.startupFollowedList = [...data];
        }, error => {
            console.clear()
        })
    }

    // Get Suggested Unions
    getSuggestedUnions() {
        this.dashboardService.getSuggestedUnions().subscribe(data => {
            this.unionSuggestedList = data;
        }, error => {
            console.clear()
        })
    }

    // Get Suggested Professors
    getSuggestedProfessors() {
        this.dashboardService.getSuggestedProfessors().subscribe(data => {
            this.professorSuggestedList = data;
        }, error => {
            console.clear()
        })
    }

    // Get Suggested StartUps
    getSuggestedStartUps() {
        this.dashboardService.getSuggestedStartUps().subscribe(data => {
            this.startupSuggestedList = data;
        }, error => {
            console.clear()
        })
    }

    // Get Most Viewed Articles
    getMostViewedArticles() {
        this.dashboardService.getMostViewedArticles().subscribe(data => {
            this.articleMostVisited = [...data]
        }, error => {
            console.clear()
        })
    }

    // Get Articles
    getArticles(role: number) {
        this.showListLoader = true;
        let data = {
            "roleId": role,
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber
        }
        this.dashboardService.getArticles(data).subscribe(data => {
            this.articleMainList = [...data]
            if (this.showLoader) {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 1500);
            }
            else {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 500);
            }
        }, error => {
            console.clear()
            if (this.showLoader) {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 1500);
            }
            else {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 500);
            }
        })
    }

    // Check Token 
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.isLogin = true;
        }, error => {
            this.isLogin = false;
            let url = "/home";
            this.router.navigate([url])
        })
    }

    // Get User Info
    getUserInfo() {
        this.publicService.getUserInfo().subscribe(data => {
            this.userData = data;
            setTimeout(() => {
                this.showLoader = false;
            }, 1000);
        }, error => {
            console.clear()
        })
    }

    ngOnInit(): void {
        // 
        this.checkToken();
        // 
        this.getUserInfo();
        // 
        this.getFollowedUnions();
        this.getFollowedProfessors();
        this.getFollowedStartUps();
        // 
        this.getSuggestedUnions();
        this.getSuggestedProfessors();
        this.getSuggestedStartUps();
        //
        this.getArticles(2);
        //
        AOS.init();
        // 
        $(document).ready(function () {
            $('.tab').click(function () {
                $('.tab').removeClass('active');
                $(this).addClass('active');
            })

        })
    }

    // get more followed list
    // union
    getMoreUnionFollowed() {
        this.getFollowedUnions();
    }
    // professor
    getMoreProfessorFollowed() {
        this.getFollowedProfessors();
    }
    // startUp
    getMoreStartupFollowed() {
        this.getFollowedStartUps();
    }

    // follow btn
    followUnion(value) {
        $('.follow-btn').click(function () {
            $(this).addClass('unFollow-btn');
            $(this).removeClass('follow-btn');
            $(this).children('a').text("دنبال شد");
        })
        this.dashboardService.followUser(value).subscribe(data => {
            this.startupFollowedList = []
            this.unionPageNumber = 0;
            this.getFollowedUnions()
        }, error => {
            console.clear()
        })

    }

    followProfessor(value) {
        $('.follow-btn').click(function () {
            $(this).addClass('unFollow-btn');
            $(this).removeClass('follow-btn');
            $(this).children('a').text("دنبال شد");
        })
        this.dashboardService.followUser(value).subscribe(data => {
            this.professorFollowedList = []
            this.professorPageNumber = 0;
            this.getFollowedProfessors()
        }, error => {
            console.clear()
        })
    }

    followStartup(value) {
        $('.follow-btn').click(function () {
            $(this).addClass('unFollow-btn');
            $(this).removeClass('follow-btn');
            $(this).children('a').text("دنبال شد");
        })
        this.dashboardService.followUser(value).subscribe(data => {
            this.startupFollowedList = []
            this.startUpPageNumber = 0;
            this.getFollowedStartUps()
        }, error => {
            console.clear()
        })
    }

    changeTab(value) {
        this.getArticles(value);
    }

}