import { Component } from "@angular/core";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import * as $ from "jquery";
import * as AOS from "aos";
import { PublicService } from "../shared/services/public.service";
import { HomeService } from "./home.service";


@Component({
    selector: 'Home-app',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent {

    public checkLogin: boolean;
    public navbarMode: boolean = false;

    faAngleLeft = faAngleLeft;
    faAngleRight = faAngleRight;

    // show loader
    public showLoader: boolean = true;

    // lists 
    public filters = [
        { "id": 1, "title": " جدید ترین ها " },
        { "id": 2, "title": " پر امتیاز ترین ها " },
        { "id": 3, "title": " پر بحث ترین ها " },
        { "id": 4, "title": " پر بازدید ترین ها " },
    ];
    public categories = [
        { "id": 1, "title": " مقاله " },
        { "id": 2, "title": " رویداد " },
        { "id": 3, "title": " اخبار " },
        { "id": 7, "title": " اطلاعیه " },
    ];

    public articles = [];
    public allEvents = [];
    public events = []

    // variables
    public currentTab: number = 1;
    public postType: any = 0;
    public category: any = 1;
    public type: any = 1;


    constructor(public publicService: PublicService, public homeService: HomeService) {
    }

    // role
    changeTab(num: number) {
        this.currentTab = num;
        if (num == 1 || num == 3 || num == 4) {
            this.categories = [
                { "id": 1, "title": " مقاله " },
                { "id": 2, "title": " رویداد " },
                { "id": 3, "title": " اخبار " },
                { "id": 7, "title": " اطلاعیه " },
            ];
        }
        else if (num == 2) {
            this.categories = [
                { "id": 4, "title": " عکس " },
                { "id": 5, "title": " ویدیو " },
                { "id": 6, "title": " پادکست " },
                { "id": 7, "title": " اطلاعیه " },
            ];
        }
        this.getMainArticles()
    }

    // type
    changeFilter(e) {
        this.type = e.srcElement.value;
        this.getMainArticles()
    }

    // post type & category
    changeCategory(e) {
        this.category = e.srcElement.value;
        if (e.srcElement.value == 7) {
            this.postType = 1;
        }
        else {
            this.postType = 0;
        }
        this.getMainArticles()
    }

    previousEvent() {
        if (this.allEvents.length != 0) {
            let item = this.allEvents.pop();
            this.events.unshift(item);
            let previousItem = this.events.pop();
            this.allEvents.unshift(previousItem);
        }
        else {
        }
    }
    nextEvent() {
        if (this.allEvents.length != 0) {
            let item = this.events.shift();
            this.allEvents.push(item);
            let nextItem = this.allEvents.shift();
            this.events.push(nextItem);
        }
        else {
        }
    }

    // get main articles
    getMainArticles() {
        let data = {
            "role": this.currentTab,
            "postType": this.category,
            "type": this.type,
            "pageSize": 6,
            "pageNumber": 1
        }
        this.homeService.getMainArticle(data).subscribe(data => {
            this.articles = []
            this.articles = data;
            setTimeout(() => {
                this.showLoader = false;
            }, 1000);
        }, error => {
        })
    }

    // get all events
    getAllEvent() {
        this.homeService.getAllEvent().subscribe(data => {
            if (data.length >= 3) {
                this.allEvents = data
                for (let i = 0; i < 3; i++) {
                    let item = this.allEvents.shift()
                    this.events.push(item)
                }
            }
            else {
                this.allEvents = []
                this.events = data
            }
        }, error => {
        })
    }

    ngOnInit(): void {

        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })

        this.getMainArticles()
        this.getAllEvent()

        AOS.init();

        $(document).ready(function () {
            $("#tab-box div").click(function () {
                $("#tab-box div").removeClass('active');
                $(this).addClass('active');
            })
        });
    }


}

