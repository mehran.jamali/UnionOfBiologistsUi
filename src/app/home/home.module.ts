// Modules
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

// Components
import { HomeComponent } from "./home.component";
import { ArticleComponent } from "./article/article.component";
import { EventComponent } from "./event/event.component";

// Services
import { HomeService } from "./home.service";

@NgModule({
  declarations: [
    HomeComponent,
    ArticleComponent,
    EventComponent
  ],
  imports: [
    SharedModule.forRoot()
  ],
  providers: [HomeService]
})
export class HomeModule { }
