import { Injectable } from "@angular/core";
import { DataService } from "../shared/services/data.service";
import { map } from "rxjs/operators"
import { Observable } from 'rxjs';

@Injectable()
export class HomeService {
    constructor(public dataService: DataService) { }

    // get main articles
    getMainArticle(data): Observable<any> {
        let url = 'api/Home/GetArticleListWithFilter';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get all event
    getAllEvent(): Observable<any> {
        let url = 'api/Home/GetAllEvents';
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }
}