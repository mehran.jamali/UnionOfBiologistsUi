import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import * as AOS from "aos";
import { PublicService } from "../../shared/services/public.service";

@Component({
    selector: 'article-app',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.scss']
})

export class ArticleComponent {
    @Input() data: any;
    @Input() postType: any;

    constructor(public router: Router, public publicService: PublicService) { }

    public imageUrl = this.publicService.urls.imageUrl;
    public defaultImageUrl = 'defaultImages/article.jpg'

    changeUrl() {
        if (this.postType == 1) {
            let url = 'notification/' + this.data.id;
            this.router.navigate([url])
        }
        else if (this.postType == 0) {
            let url = 'article/' + this.data.id;
            this.router.navigate([url])
        }
    }


    ngOnInit(): void {
        AOS.init();
    }
}