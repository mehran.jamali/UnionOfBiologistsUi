import { Component, Input } from "@angular/core";
import { faHeart, faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import { PublicService } from "../../shared/services/public.service";

@Component({
    selector: 'event-app',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss']
})

export class EventComponent {

    // images
    public imageUrl = this.publicService.urls.imageUrl;
    public defaultImageUrl = 'defaultImages/article.jpg';

    // data
    @Input() data: any;

    // icons
    faCalendar = faCalendarAlt;
    faHeart = faHeart;

    constructor(public publicService: PublicService) { }

    ngOnInit(): void {
        AOS.init();
    }
}