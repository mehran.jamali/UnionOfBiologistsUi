import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { faHome, faAngleLeft, faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { HttpClient } from "@angular/common/http";
import { PublicService } from "../../../shared/services/public.service";

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit {
  // select value
  public selectValues = { "fieldId": 0, "universityId": 0, "provinceId": 0, "educationCityId": 0, "educationDegreeId": 0 }

  // connection error
  public connectionError: boolean = false;

  // check file size
  public profileImageSizeIsValid = false;
  public coverImageSizeIsValid = false;

  // send data to parent component
  @Output() goToMainRegister = new EventEmitter();
  @Output() getUserDetail = new EventEmitter();

  // icons
  faHome = faHome
  faAngleLeft = faAngleLeft
  faLongArrowAltLeft = faLongArrowAltLeft

  // images
  coverFileData: File = null;
  coverPreviewUrl: any = null;
  profileFileData: File = null;
  profilePreviewUrl: any = null;
  public coverImagePicked: boolean = false;
  public profileImagePicked: boolean = false;


  // user detail form
  public userDetailForm = new FormGroup({
    field: new FormControl("", [Validators.required]),
    aboutMe: new FormControl("", [Validators.required]),
    province: new FormControl("", [Validators.required]),
    university: new FormControl({ value: '', disabled: true }, [Validators.required]),
    city: new FormControl({ value: '', disabled: true }, [Validators.required]),
    degree: new FormControl('', [Validators.required]),
  })

  // form controls
  get userDetailFormControls() { return this.userDetailForm.controls; }




  // list
  public fields = [];
  public universities = [];
  public provinceList = [];
  public cities = [];
  public degrees = [];

  public filteredFields;
  public filteredUniversities;
  public filteredProvinces;
  public filteredCities;
  public filteredDegrees;


  // select field
  setFieldValue(value) {
    this.selectValues.fieldId = value.id;
    this.userDetailForm.controls.field.setValue(value.name);
    this.filteredFields = this.fields;
  }

  // select province
  setProvinceValue(value) {
    this.selectValues.provinceId = value.id;
    this.userDetailForm.controls.province.setValue(value.name);
    this.userDetailForm.controls['city'].enable();
    this.filteredProvinces = this.provinceList;
    this.getCityByProvinceId(value.id)
  }

  // select city
  setCityValue(value) {
    this.selectValues.educationCityId = value.id;
    this.userDetailForm.controls.city.setValue(value.name);
    this.userDetailForm.controls['university'].enable();
    this.filteredCities = this.cities;
    this.getUniversityByCityId(value.id)
  }

  // select university
  setUniversityValue(value) {
    this.selectValues.universityId = value.id;
    this.userDetailForm.controls.university.setValue(value.name);
    this.filteredUniversities = this.universities;
  }

  // select degree
  setDegreeValue(value) {
    this.selectValues.educationDegreeId = value.id;
    this.userDetailForm.controls.degree.setValue(value.title);
    this.filteredDegrees = this.degrees;
  }

  // filter fields 
  filterFields() {
    this.filteredFields = this.fields.filter(x => {
      return x.name.includes(this.userDetailForm.value.field);
    })
  }
  // filter province 
  filterProvinces() {
    this.filteredProvinces = this.provinceList.filter(x => {
      return x.name.includes(this.userDetailForm.value.province);
    })
  }
  // filter city 
  filterCities() {
    this.filteredCities = this.cities.filter(x => {
      return x.name.includes(this.userDetailForm.value.city);
    })
  }
  // filter university 
  filterUniversity() {
    this.filteredUniversities = this.universities.filter(x => {
      return x.name.includes(this.userDetailForm.value.university);
    })
  }
  // filter degree 
  filterDegree() {
    this.filteredDegrees = this.degrees.filter(x => {
      return x.title.includes(this.userDetailForm.value.degree);
    })
  }

  // go back to register main
  goMainRegister() {
    this.goToMainRegister.emit()
  }

  // send data to register component
  sendUserDetailDetail() {
    let data = {
      "fieldId": this.selectValues.fieldId,
      "aboutMe": this.userDetailForm.value.aboutMe,
      "universityId": this.selectValues.universityId,
      "educationCityId": this.selectValues.educationCityId,
      "educationDegreeId": this.selectValues.educationDegreeId,
      "coverImage": this.coverFileData,
      "profileImage": this.profileFileData
    }
    this.getUserDetail.emit(data);
  }

  constructor(private http: HttpClient, public publicService: PublicService) { }

  // get province
  getProvince() {
    this.publicService.getProvince().subscribe(data => {
      this.provinceList = data;
      this.filteredProvinces = this.provinceList;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  // get city by province id
  getCityByProvinceId(id) {
    this.publicService.getCityByProvinceId(id).subscribe(data => {
      this.cities = data;
      this.filteredCities = this.cities;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  // get university by city id
  getUniversityByCityId(id) {
    this.publicService.getUniversitiesByCityId(id).subscribe(data => {
      this.universities = data;
      this.filteredUniversities = this.universities;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    }
    )
  }

  // get education degree
  getEducationDegree() {
    this.publicService.getEducationDegree().subscribe(data => {
      this.degrees = data;
      this.filteredDegrees = this.degrees;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  // get fields
  getFields() {
    this.publicService.getFields().subscribe(data => {
      this.fields = data;
      this.filteredFields = this.fields;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    }
    )
  }

  ngOnInit() {

    // get data 
    this.getProvince();
    this.getEducationDegree();
    this.getFields()

    AOS.init();
    $(document).ready(function () {
      // open dropdown
      $(".select-input").focus(function () {
        $("#" + $(this).attr('name')).addClass("show-dropdown");
      })
      // close dropdown
      $(".select-input").focusout(function () {
        $("#" + $(this).attr('name')).removeClass("show-dropdown");
      })

    })
  }

  coverFileProgress(fileInput: any) {
    this.coverFileData = <File>fileInput.target.files[0];
    this.coverImageSizeIsValid = this.coverFileData.size < 5120000 ? true : false;
    this.coverPreview();
    this.coverImagePicked = true;
  }

  coverPreview() {
    // Show preview 
    let mimeType = this.coverFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.coverFileData);
    reader.onload = (_event) => {
      this.coverPreviewUrl = reader.result;
    }
  }

  uploadCoverImage() {
    let data = { "file": this.coverFileData };
    console.log(data);
    // this.http.post('url/to/your/api', this.coverFormData)
    //   .subscribe(res => {
    //     console.log(res);
    //     alert('SUCCESS !!');
    //   })
  }


  profileFileProgress(fileInput: any) {
    this.profileFileData = <File>fileInput.target.files[0];
    this.profileImageSizeIsValid = this.profileFileData.size < 5120000 ? true : false;
    this.profilePreview();
    this.profileImagePicked = true;
  }

  profilePreview() {
    // Show preview 
    let mimeType = this.profileFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.profileFileData);
    reader.onload = (_event) => {
      this.profilePreviewUrl = reader.result;
    }
  }


}
