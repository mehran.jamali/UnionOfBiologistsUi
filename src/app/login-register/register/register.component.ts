import { Component, OnInit } from '@angular/core';

import { LoginRegisterService } from "../login-register.service";
import { SharedService } from '../../shared/shared.service';
import { PublicService } from "../../shared/services/public.service";


import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";

import { faHome } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  // icons 
  faHome = faHome;

  // check 
  public role = '';

  // check register error
  public registerError = false;

  // register data
  public registerData: any;

  // detail data
  public detailData: any;

  constructor(public sharedService: SharedService, public service: LoginRegisterService, public http: HttpClient, public router: Router, public publicService: PublicService) { }

  // do register
  doRegister() {
    this.service.doRegister(this.registerData).subscribe((response) => {
      if (response) {
        localStorage.setItem("token", response.token);
        localStorage.setItem("role", response.role);
        localStorage.setItem("id", response.id);
        this.setUserInfo()
        if (this.role == '3bd2452a-c1d4-4638-8946-d3fd0e8edc83') {
          this.sendUnionDetail()
        }
        else if (this.role == '31f8313f-3f9c-47a1-9127-1eb0c6e28396') {
          this.sendProfessorDetail()
        }
        else if (this.role == 'a820aef8-fb66-4e0b-8021-741a045f8c1e') {
          this.sendUserDetail()
        }
        else if (this.role == '7e9c1771-353c-474e-8ff8-deaaa3514e7e') {
          this.sendStartupDetail()
        }
      }
    }, error => {
      if (error.status == 400) {
        this.registerError = true
        this.role = ''
        $(".main").removeClass('hide');
      }
    })
  }


  // set user info
  setUserInfo() {
    let url = "api/Account/GetUserInfo";
    this.publicService.getUserInfo().subscribe(data => {
      this.sharedService.setProfileData(data);
    }, error => {
      console.log(error);
    })
  }

  // manage components
  register(event: any) {
    console.log(event);
    this.registerData = event;
    $(".main").addClass('hide');
    this.role = event.roleId;
  }

  // go main register
  goToMainRegister() {
    $(".main").removeClass('hide');
    this.role = '';
  }

  // get data from union component
  getUnionDetail(event: any) {
    this.detailData = event;
    this.doRegister();
  }

  // get data from professor component
  getProfessorDetail(event: any) {
    this.detailData = event;
    this.doRegister();
  }

  // get data from startup component
  getStartupDetail(event: any) {
    this.detailData = event;
    this.doRegister();
  }

  // get data from user component
  getUserDetail(event: any) {
    this.detailData = event;
    this.doRegister();
  }

  // send union detail
  sendUnionDetail() {
    this.service.doSendUnionDetail(this.detailData).subscribe(data => {
      if (data) {
        let id = data.id;
        let coverImageUrl = `Union/api/UnionDetail/AddCoverImage/${id}`
        let profileImageUrl = `Union/api/UnionDetail/AddProfileImage/${id}`
        this.service.doUploadProfileImage(profileImageUrl, this.detailData.profileImage).subscribe(data => {
          // do nothing
        }, error => {
          console.log(error.status);
        })
        this.service.doUploadCoverImage(coverImageUrl, this.detailData.coverImage).subscribe(data => {
          this.router.navigate(['home'])
        }, error => {
          console.log(error.status);
        })
      }
    })
  }

  // send startup detail
  sendStartupDetail() {
    this.service.doSendStartupDetail(this.detailData).subscribe(data => {
      if (data) {
        let id = data.id;
        let coverImageUrl = `StartUp/api/StartUpDetail/AddCoverImage/${id}`
        let profileImageUrl = `StartUp/api/StartUpDetail/AddProfileImage/${id}`
        this.service.doUploadProfileImage(profileImageUrl, this.detailData.profileImage).subscribe(data => {
          // do nothing
        }, error => {
          console.log(error.status);
        })
        this.service.doUploadCoverImage(coverImageUrl, this.detailData.coverImage).subscribe(data => {
          this.router.navigate(['home'])
        }, error => {
          console.log(error.status);
        })
      }
    })
  }

  // send User detail
  sendUserDetail() {
    this.service.doSendUserDetail(this.detailData).subscribe(data => {
      if (data) {
        let id = data.id;
        let coverImageUrl = `User/api/UserDetail/AddCoverImage/${id}`
        let profileImageUrl = `User/api/UserDetail/AddProfileImage/${id}`
        this.service.doUploadProfileImage(profileImageUrl, this.detailData.profileImage).subscribe(data => {
          // do nothing
        }, error => {
          console.log(error.status);
        })
        this.service.doUploadCoverImage(coverImageUrl, this.detailData.coverImage).subscribe(data => {
          this.router.navigate(['home'])
        }, error => {
          console.log(error.status);
        })
      }
    })
  }

  // send professor detail
  sendProfessorDetail() {
    this.service.doSendProfessorDetail(this.detailData).subscribe(data => {
      if (data) {
        let id = data.id;
        let profileImageUrl = `Professor/api/ProfessorDetail/AddProfileImage/${id}`
        let CVUrl = `Professor/api/ProfessorDetail/AddCV/${id}`
        this.service.doUploadProfileImage(profileImageUrl, this.detailData.profileImage).subscribe(data => {
          // do nothing
        }, error => {
          console.log(error.status);
        })
        this.service.doUploadResumeFile(CVUrl, this.detailData.resumeFile).subscribe(data => {
          this.router.navigate(['home'])
        }, error => {
          console.log(error.status);
        })
      }
    })
  }

  ngOnInit() {
    AOS.init();
    $(document).ready(function () { });
  }

}
