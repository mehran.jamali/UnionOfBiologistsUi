import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { faHome, faAngleLeft, faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { HttpClient } from "@angular/common/http";
import { PublicService } from "../../../shared/services/public.service";


@Component({
  selector: 'app-union-register',
  templateUrl: './union-register.component.html',
  styleUrls: ['./union-register.component.scss']
})
export class UnionRegisterComponent implements OnInit {

  // connection error
  public connectionError: boolean = false;

  // select value
  public selectValues = { "unionType": 0, "fieldId": 0, "universityId": 0 }

  // check file size
  public profileImageSizeIsValid = false;
  public coverImageSizeIsValid = false;

  // send data to parent component
  @Output() goToMainRegister = new EventEmitter();
  @Output() getUnionDetail = new EventEmitter();

  // icons
  faHome = faHome
  faAngleLeft = faAngleLeft
  faLongArrowAltLeft = faLongArrowAltLeft

  // images
  coverFileData: File = null;
  coverPreviewUrl: any = null;
  profileFileData: File = null;
  profilePreviewUrl: any = null;
  public coverImagePicked: boolean = false;
  public profileImagePicked: boolean = false;


  // union detail form
  public unionDetailForm = new FormGroup({
    name: new FormControl("", [Validators.required]),
    unionType: new FormControl("", [Validators.required]),
    field: new FormControl("", [Validators.required]),
    establishmentYear: new FormControl("", [Validators.required]),
    aboutUnion: new FormControl("", [Validators.required]),
    university: new FormControl("", [Validators.required]),
    address: new FormControl('', [Validators.required]),
    telegram: new FormControl(''),
    slogan: new FormControl(''),
  })

  // form controls
  get unionDetailFormControls() { return this.unionDetailForm.controls; }



  public unionTypes = [
    { "id": 1, "name": 'دانشگاهی' },
    { "id": 2, "name": 'کشوری' },
    { "id": 3, "name": 'فرهنگی هنری' },
  ];
  public fields = []
  public universities = []

  public filteredUnionTypes = this.unionTypes;
  public filteredFields;
  public filteredUniversities;


  // select union type
  setUnionTypeValue(value) {
    this.selectValues.unionType = value.id;
    this.unionDetailForm.controls.unionType.setValue(value.name);
    this.filteredUnionTypes = this.unionTypes
  }

  // select field
  setFieldValue(value) {
    this.selectValues.fieldId = value.id;
    this.unionDetailForm.controls.field.setValue(value.name);
    this.filteredFields = this.fields
  }

  // set role 
  setUniversityValue(value) {
    this.selectValues.universityId = value.id;
    this.unionDetailForm.controls.university.setValue(value.name);
    this.filteredUniversities = this.universities
  }

  // filter union type
  filterUnionType() {
    this.filteredUnionTypes = this.unionTypes.filter(x => {
      return x.name.includes(this.unionDetailForm.value.unionType);
    })
  }

  // filter field
  filterField() {
    this.filteredFields = this.fields.filter(x => {
      return x.name.includes(this.unionDetailForm.value.field);
    })
  }

  // filter university
  filterUniversity() {
    this.filteredUniversities = this.universities.filter(x => {
      return x.name.includes(this.unionDetailForm.value.university);
    })
  }




  // go back to register main
  goMainRegister() {
    this.goToMainRegister.emit()
  }

  // send data to register component
  sendUnionDetail() {
    let data = {
      "name": this.unionDetailForm.value.name,
      "unionType": this.selectValues.unionType,
      "fieldId": this.selectValues.fieldId,
      "establishmentYear": this.unionDetailForm.value.establishmentYear,
      "aboutUnion": this.unionDetailForm.value.aboutUnion,
      "universityId": this.selectValues.universityId,
      "address": this.unionDetailForm.value.address,
      "telegram": this.unionDetailForm.value.telegram,
      "slogan": this.unionDetailForm.value.slogan,
      "coverImage": this.coverFileData,
      "profileImage": this.profileFileData
    }
    this.getUnionDetail.emit(data);
  }

  constructor(private http: HttpClient, public publicService: PublicService) { }

  // get university
  getUniversity() {
    this.publicService.getUniversities().subscribe(data => {
      this.universities = data;
      this.filteredUniversities = this.universities;
      this.connectionError = false;
    }, error => {
      this.connectionError = true
    })
  }

  // get field
  getFields() {
    this.publicService.getFields().subscribe(data => {
      this.fields = data;
      this.filteredFields = this.fields;
      this.connectionError = false;
    }, error => {
      this.connectionError = true
    })
  }

  ngOnInit() {

    // get data
    this.getFields()
    this.getUniversity()

    AOS.init();
    $(document).ready(function () {
      // open dropdown
      $(".select-input").focus(function () {
        $("#" + $(this).attr('name')).addClass("show-dropdown");
      })
      // close dropdown
      $(".select-input").focusout(function () {
        $("#" + $(this).attr('name')).removeClass("show-dropdown");
      })

    })
  }





  coverFileProgress(fileInput: any) {
    this.coverFileData = <File>fileInput.target.files[0];
    this.coverImageSizeIsValid = this.coverFileData.size < 5120000 ? true : false;
    this.coverPreview();
    this.coverImagePicked = true;
  }

  coverPreview() {
    // Show preview 
    let mimeType = this.coverFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.coverFileData);
    reader.onload = (_event) => {
      this.coverPreviewUrl = reader.result;
    }
  }

  uploadCoverImage() {
    let data = { "file": this.coverFileData };
    console.log(data);
    // this.http.post('url/to/your/api', this.coverFormData)
    //   .subscribe(res => {
    //     console.log(res);
    //     alert('SUCCESS !!');
    //   })
  }


  profileFileProgress(fileInput: any) {
    this.profileFileData = <File>fileInput.target.files[0];
    this.profileImageSizeIsValid = this.profileFileData.size < 5120000 ? true : false;
    this.profilePreview();
    this.profileImagePicked = true;
  }

  profilePreview() {
    // Show preview 
    let mimeType = this.profileFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.profileFileData);
    reader.onload = (_event) => {
      this.profilePreviewUrl = reader.result;
    }
  }


}
