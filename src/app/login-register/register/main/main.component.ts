// other
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { Router } from '@angular/router';

// service
import { LoginRegisterService } from "../../login-register.service";
import { PublicService } from "../../../shared/services/public.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  // register error , from parent component
  @Input() registerError;

  // connection error
  public connectionError: boolean = false;

  // icons 
  faHome = faHome;

  // check password
  public passwordIsMatch = false;
  public passwordHaveNumber = false;

  // check phone number
  public phoneNumberIsValid = false;

  // select value
  public selectValues = { "cityId": 0, "roleId": 0 }

  // output => send data to parent component 
  @Output() doRegister = new EventEmitter();

  public provinces = [];

  public cities = []

  public roleList = []

  public filteredProvinces;
  public filteredCities;
  public filteredRoles;

  constructor(public Service: LoginRegisterService, public router: Router, public publicService: PublicService) { }

  public registerForm = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [Validators.required, Validators.minLength(8)]),
    confirmedPassword: new FormControl("", [Validators.required, Validators.minLength(8)]),
    name: new FormControl("", [Validators.required]),
    family: new FormControl("", [Validators.required]),
    province: new FormControl("", [Validators.required]),
    city: new FormControl({ value: '', disabled: true }, [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required]),
    role: new FormControl('', [Validators.required])
  })

  get registerFormControls() { return this.registerForm.controls; }


  // select province
  setProvinceValue(value) {
    this.registerForm.controls.province.setValue(value.name);
    this.registerForm.controls['city'].enable();
    this.filteredProvinces = this.provinces;
    this.getCityByProvinceId(value.id)

  }

  // select city
  setCityValue(value) {
    this.selectValues.cityId = value.id;
    this.registerForm.controls.city.setValue(value.name);
    this.filteredCities = this.cities;
  }

  // set role 
  setRoleValue(value) {
    this.selectValues.roleId = value.id;
    this.registerForm.controls.role.setValue(value.name);
    this.filteredRoles = this.roleList;
  }

  // filter province
  filterProvince() {
    this.filteredProvinces = this.provinces.filter(x => {
      return x.name.includes(this.registerForm.value.province);
    })
  }

  // filter city
  filterCity() {
    this.filteredCities = this.cities.filter(x => {
      return x.name.includes(this.registerForm.value.city);
    })
  }

  // filter role
  filterRole() {
    this.filteredRoles = this.roleList.filter(x => {
      return x.name.includes(this.registerForm.value.role);
    })
  }

  // check password
  checkPassowrd() {
    if (this.registerForm.value.password == this.registerForm.value.confirmedPassword) {
      this.passwordIsMatch = true;
    }
    else {
      this.passwordIsMatch = false;
    }

    let regex = /\d+/g;
    this.passwordHaveNumber = this.registerForm.value.password.match(regex) ? true : false;

  }

  // check phone number
  checkPhoneNumber() {
    let value = "0" + this.registerForm.value.phoneNumber.toString()
    let regex = /(0){0,2}9[1|2|3|4]{0,2}(?:[0-9]{0,2}){8}/ig
    if (value.toString().length == 11) {
      this.phoneNumberIsValid = value.match(regex) ? true : false;
    }
    else {
      this.phoneNumberIsValid = false;
    }
  }

  // check email
  checkEmail() {
    this.registerError = false;
  }

  // register 
  register() {
    let data = {
      "userName": this.registerForm.value.email.toString(),
      "password": this.registerForm.value.password.toString(),
      "name": this.registerForm.value.name,
      "family": this.registerForm.value.family,
      "cityId": this.selectValues.cityId,
      "phoneNumber": "0" + this.registerForm.value.phoneNumber.toString(),
      "roleId": this.selectValues.roleId.toString()
    }
    this.doRegister.emit(data);
  }

  // get province
  getProvince() {
    this.publicService.getProvince().subscribe(data => {
      this.provinces = data;
      this.filteredProvinces = this.provinces;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  // get city by province id
  getCityByProvinceId(id) {
    this.publicService.getCityByProvinceId(id).subscribe(data => {
      this.cities = data;
      this.filteredCities = this.cities;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }


  // get roles
  getRoles() {
    this.publicService.getRoles().subscribe(data => {
      this.roleList = data;
      this.filteredRoles = this.roleList
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  ngOnInit() {

    // get province 
    this.getProvince();
    this.getRoles()

    AOS.init();
    $(document).ready(function () {
      // open dropdown
      $(".select-input").focus(function () {
        $("#" + $(this).attr('name')).addClass("show-dropdown");
      })
      // close dropdown
      $(".select-input").focusout(function () {
        $("#" + $(this).attr('name')).removeClass("show-dropdown");
      })

    })
  }


}