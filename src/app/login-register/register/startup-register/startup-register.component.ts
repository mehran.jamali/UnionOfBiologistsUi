import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { faHome, faAngleLeft, faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-startup-register',
  templateUrl: './startup-register.component.html',
  styleUrls: ['./startup-register.component.scss']
})
export class StartupRegisterComponent implements OnInit {

  // connection error
  public connectionError: boolean = false;

  // select value
  public selectValues = { "activityId": 0 }

  // check file size
  public profileImageSizeIsValid = false;
  public coverImageSizeIsValid = false;

  // send data to parent component
  @Output() goToMainRegister = new EventEmitter();
  @Output() getStartupDetail = new EventEmitter();

  // icons
  faHome = faHome
  faAngleLeft = faAngleLeft
  faLongArrowAltLeft = faLongArrowAltLeft

  // images
  coverFileData: File = null;
  coverPreviewUrl: any = null;
  profileFileData: File = null;
  profilePreviewUrl: any = null;
  public coverImagePicked: boolean = false;
  public profileImagePicked: boolean = false;


  // startup detail form
  public startupDetailForm = new FormGroup({
    name: new FormControl("", [Validators.required]),
    activity: new FormControl("", [Validators.required]),
    establishmentYear: new FormControl("", [Validators.required]),
    aboutStartUp: new FormControl("", [Validators.required]),
    address: new FormControl('', [Validators.required]),
    telegram: new FormControl(''),
    slogan: new FormControl(''),
  })

  // form controls
  get startupDetailFormControls() { return this.startupDetailForm.controls; }



  public activities = [
    { "id": 1, "name": 'تولیدی' },
    { "id": 2, "name": 'پژوهشی' },
    { "id": 3, "name": 'آموزشی' },
  ];

  public filteredActivites = this.activities;


  // select activity
  setActivityValue(value) {
    this.selectValues.activityId = value.id;
    this.startupDetailForm.controls.activity.setValue(value.name);
  }

  // filter activity
  filterActivity() {
    this.filteredActivites = this.activities.filter(x => {
      return x.name.includes(this.startupDetailForm.value.activity);
    })
  }

  // go back to register main
  goMainRegister() {
    this.goToMainRegister.emit()
  }

  // send data to register component
  sendStartupDetail() {
    let data = {
      "name": this.startupDetailForm.value.name,
      "activity": this.selectValues.activityId,
      "establishmentYear": this.startupDetailForm.value.establishmentYear,
      "aboutStartUp": this.startupDetailForm.value.aboutStartUp,
      "address": this.startupDetailForm.value.address,
      "telegram": this.startupDetailForm.value.telegram,
      "slogan": this.startupDetailForm.value.slogan,
      "coverImage": this.coverFileData,
      "profileImage": this.profileFileData
    }
    this.getStartupDetail.emit(data);
  }

  constructor(private http: HttpClient) { }

  ngOnInit() {
    AOS.init();
    $(document).ready(function () {
      // open dropdown
      $(".select-input").focus(function () {
        $("#" + $(this).attr('name')).addClass("show-dropdown");
      })
      // close dropdown
      $(".select-input").focusout(function () {
        $("#" + $(this).attr('name')).removeClass("show-dropdown");
      })

    })
  }

  coverFileProgress(fileInput: any) {
    this.coverFileData = <File>fileInput.target.files[0];
    this.coverImageSizeIsValid = this.coverFileData.size < 5120000 ? true : false;
    this.coverPreview();
    this.coverImagePicked = true;
  }

  coverPreview() {
    // Show preview 
    let mimeType = this.coverFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.coverFileData);
    reader.onload = (_event) => {
      this.coverPreviewUrl = reader.result;
    }
  }

  uploadCoverImage() {
    let data = { "file": this.coverFileData };
    console.log(data);
    // this.http.post('url/to/your/api', this.coverFormData)
    //   .subscribe(res => {
    //     console.log(res);
    //     alert('SUCCESS !!');
    //   })
  }


  profileFileProgress(fileInput: any) {
    this.profileFileData = <File>fileInput.target.files[0];
    this.profileImageSizeIsValid = this.profileFileData.size < 5120000 ? true : false;
    this.profilePreview();
    this.profileImagePicked = true;
  }

  profilePreview() {
    // Show preview 
    let mimeType = this.profileFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.profileFileData);
    reader.onload = (_event) => {
      this.profilePreviewUrl = reader.result;
    }
  }


}
