import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { faHome, faAngleLeft, faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { HttpClient } from "@angular/common/http";
import { PublicService } from "../../../shared/services/public.service";

@Component({
  selector: 'app-professor-register',
  templateUrl: './professor-register.component.html',
  styleUrls: ['./professor-register.component.scss']
})
export class ProfessorRegisterComponent implements OnInit {

  // connection error
  public connectionError: boolean = false;

  // select value
  public selectValues = { "educationDegreeId": 0, "fieldId": 0, "universityId": 0, "academicRank": 0 }

  // check file size
  public imageSizeIsValid = false;
  public resumeSizeIsValid = false;

  // send data to parent component
  @Output() goToMainRegister = new EventEmitter();
  @Output() getProfessorDetail = new EventEmitter();

  // icons
  faHome = faHome
  faAngleLeft = faAngleLeft
  faLongArrowAltLeft = faLongArrowAltLeft

  // files
  profileFileData: File = null;
  profilePreviewUrl: any = null;
  public profileImagePicked: boolean = false;

  resumeFileData: File = null;
  public resumeFilePicked: boolean = false;


  // union detail form
  public professorDetailForm = new FormGroup({
    academicRank: new FormControl("", [Validators.required]),
    field: new FormControl("", [Validators.required]),
    educationDegree: new FormControl("", [Validators.required]),
    university: new FormControl("", [Validators.required]),
    collegeName: new FormControl("", [Validators.required]),
    birthYear: new FormControl("", [Validators.required]),
    linkedIn: new FormControl(''),
    twitter: new FormControl(''),
    facebook: new FormControl(''),
    instagram: new FormControl(''),
    telegram: new FormControl(''),
  })

  // form controls
  get professorDetailFormControls() { return this.professorDetailForm.controls; }



  public academicRanks = [
    { "id": 1, "name": 'استاد' },
    { "id": 2, "name": 'دانشیار' },
    { "id": 3, "name": 'استادیار' },
    { "id": 4, "name": 'مربی' },
  ];

  public educationDegrees = []

  public fields = []

  public universities = []

  public filteredAcademicRank = this.academicRanks;
  public filteredFields;
  public filteredUniversities;
  public filteredEducationDegrees;


  // select professor academic rank
  setAcademicRankValue(value) {
    this.selectValues.academicRank = value.id;
    this.professorDetailForm.controls.academicRank.setValue(value.name);
    this.filteredAcademicRank = this.academicRanks;
  }


  // select professor education degree
  setEducationDegreeValue(value) {
    this.selectValues.educationDegreeId = value.id;
    this.professorDetailForm.controls.educationDegree.setValue(value.title);
    this.filteredEducationDegrees = this.educationDegrees;
  }

  // select field
  setFieldValue(value) {
    this.selectValues.fieldId = value.id;
    this.professorDetailForm.controls.field.setValue(value.name);
    this.filteredFields = this.fields;
  }

  // set university 
  setUniversityValue(value) {
    this.selectValues.universityId = value.id;
    this.professorDetailForm.controls.university.setValue(value.name);
    this.filteredUniversities = this.universities;
  }

  // filter academic rank
  filterAcademicRank() {
    this.filteredAcademicRank = this.academicRanks.filter(x => {
      return x.name.includes(this.professorDetailForm.value.academicRank);
    })
  }

  // filter education degree 
  filterEducationDegree() {
    this.filteredEducationDegrees = this.educationDegrees.filter(x => {
      return x.title.includes(this.professorDetailForm.value.educationDegree);
    })
  }

  // filter field
  filterField() {
    this.filteredFields = this.fields.filter(x => {
      return x.name.includes(this.professorDetailForm.value.field);
    })
  }

  // filter university
  filterUniversity() {
    this.filteredUniversities = this.universities.filter(x => {
      return x.name.includes(this.professorDetailForm.value.university);
    })
  }

  // go back to register main
  goMainRegister() {
    this.goToMainRegister.emit()
  }

  // send data to register component
  sendProfessorDetail() {
    let data = {
      "academicRank": this.selectValues.academicRank,
      "field": this.selectValues.fieldId,
      "educationDegree": this.selectValues.educationDegreeId,
      "university": this.selectValues.universityId,
      "collegeName": this.professorDetailForm.value.collegeName,
      "birthYear": this.professorDetailForm.value.birthYear,
      "linkedIn": this.professorDetailForm.value.linkedIn,
      "twitter": this.professorDetailForm.value.twitter,
      "facebook": this.professorDetailForm.value.facebook,
      "instagram": this.professorDetailForm.value.instagram,
      "telegram": this.professorDetailForm.value.telegram,
      "profileImage": this.profileFileData,
      "resumeFile": this.resumeFileData
    }
    this.getProfessorDetail.emit(data);
  }

  constructor(private http: HttpClient, public publicService: PublicService) { }

  // get university
  getUniversity() {
    this.publicService.getUniversities().subscribe(data => {
      this.universities = data;
      this.filteredUniversities = this.universities;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  // get education degree
  getEducationDegree() {
    this.publicService.getEducationDegree().subscribe(data => {
      this.educationDegrees = data;
      this.filteredEducationDegrees = this.educationDegrees;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }

  // get field
  getFields() {
    this.publicService.getFields().subscribe(data => {
      this.fields = data;
      this.filteredFields = this.fields;
      this.connectionError = false
    }, error => {
      this.connectionError = true
    })
  }


  ngOnInit() {

    // get data
    this.getUniversity()
    this.getEducationDegree()
    this.getFields()

    AOS.init();
    $(document).ready(function () {
      // open dropdown
      $(".select-input").focus(function () {
        $("#" + $(this).attr('name')).addClass("show-dropdown");
      })
      // close dropdown
      $(".select-input").focusout(function () {
        $("#" + $(this).attr('name')).removeClass("show-dropdown");
      })

    })
  }






  // profile image upload

  profileFileProgress(fileInput: any) {
    this.profileFileData = <File>fileInput.target.files[0];
    this.imageSizeIsValid = this.profileFileData.size < 5120000 ? true : false;
    this.profileImagePicked = true;
    this.profilePreview();
  }

  profilePreview() {
    // Show preview 
    let mimeType = this.profileFileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(this.profileFileData);
    reader.onload = (_event) => {
      this.profilePreviewUrl = reader.result;
    }
  }

  uploadProfileImage() {
    let data = { "file": this.profileFileData };
    console.log(data);
    // this.http.post('url/to/your/api', this.coverFormData)
    //   .subscribe(res => {
    //     console.log(res);
    //     alert('SUCCESS !!');
    //   })
  }


  // resume upload

  resumeFileProgress(fileInput: any) {
    this.resumeFileData = <File>fileInput.target.files[0];
    this.resumeSizeIsValid = this.resumeFileData.size < 5120000 ? true : false;
    this.resumeFilePicked = true;
    $('.resume-box').addClass('resume-selected');
  }


}
