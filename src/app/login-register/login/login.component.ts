import { Component, OnInit } from '@angular/core';
import { LoginRegisterService } from "../login-register.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import { Router } from '@angular/router';
import { SharedService } from '../../shared/shared.service';
import { PublicService } from "../../shared/services/public.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // public userList: any;
  faHome = faHome;

  // url
  public previousUrl = ""

  // show login error
  public loginError = false;

  constructor(public publicService: PublicService, public Service: LoginRegisterService, public router: Router, public sharedService: SharedService) { }

  public loginForm = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [Validators.required])
  })

  get loginFormControls() { return this.loginForm.controls; }

  // do login
  login() {
    this.previousUrl = this.sharedService.getPreviousUrl();
    let data = {
      "userName": this.loginForm.value.email,
      "password": this.loginForm.value.password
    }
    this.Service.doLogin(data).subscribe(data => {
      localStorage.setItem("token", data.token);
      localStorage.setItem("role", data.role);
      localStorage.setItem("id", data.id);
      this.setUserInfo()

    }, error => {
      this.loginError = true;
    })
  }

  // set user info
  setUserInfo() {
    this.publicService.getUserInfo().subscribe(data => {
      this.sharedService.setProfileData(data);
      this.previousUrl != "" ? this.router.navigate([this.previousUrl]) : this.router.navigate(['home']);
    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    AOS.init();
    // this.createUser()
    // this.deleteUser();
    // this.Service.getUserList().subscribe(data => {
    //   this.userList = data;
    //   console.log(this.userList);
    // })
  }

  // createUser() {
  //   let newUser = {
  //     'name': 'mehran',
  //     'job': 'programmer'
  //   }
  //   this.Service.createUser(newUser).subscribe(x => console.log("user created!"))
  // }

  // deleteUser() {
  //   let id = 552;
  //   this.Service.deleteUser(id).subscribe(x => console.log("user Deleted!"))
  // }



}
