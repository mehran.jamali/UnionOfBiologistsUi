// Modules
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

// Components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

// Service
import { LoginRegisterService } from './login-register.service';
import { MainComponent } from './register/main/main.component';
import { UnionRegisterComponent } from './register/union-register/union-register.component';
import { ProfessorRegisterComponent } from './register/professor-register/professor-register.component';
import { UserRegisterComponent } from './register/user-register/user-register.component';
import { StartupRegisterComponent } from './register/startup-register/startup-register.component';


@NgModule({
  declarations: [LoginComponent, RegisterComponent, MainComponent, UnionRegisterComponent, ProfessorRegisterComponent, UserRegisterComponent, StartupRegisterComponent],
  imports: [
    SharedModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [LoginRegisterService]

})
export class LoginRegisterModule { }
