import { Injectable } from "@angular/core";
import { DataService } from "../shared/services/data.service";
import { Observable } from "rxjs"
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class LoginRegisterService {
    constructor(public service: DataService) { }

    // login
    doLogin(data): Observable<any> {
        let url = 'api/Account/Login';
        return this.service.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // register
    doRegister(data): Observable<any> {
        let url = 'api/Account/Register';
        return this.service.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // send union detail
    doSendUnionDetail(data): Observable<any> {
        let url = "Union/api/UnionDetail";
        let unionData = {
            "name": data.name,
            "unionType": data.unionType,
            "fieldId": data.fieldId,
            "establishmentYear": data.establishmentYear,
            "aboutUnion": data.aboutUnion,
            "universityId": data.universityId,
            "address": data.address,
            "telegram": data.telegram,
            "slogan": data.slogan
        }
        return this.service.post(url, unionData).pipe(map((response: any) => {
            return response
        }))
    }

    // send professor datail
    doSendProfessorDetail(data): Observable<any> {
        let url = "Professor/api/ProfessorDetail"
        let professorData = {
            "fieldId": data.field,
            "academicRank": data.academicRank,
            "educationDegreeId": data.educationDegree,
            "universityId": data.university,
            "collegeName": data.collegeName,
            "birthYear": data.birthYear,
            "linkedIn": data.linkedIn,
            "twitter": data.twitter,
            "facebook": data.facebook,
            "instagram": data.instagram,
            "telegram": data.telegram
        }
        return this.service.post(url, professorData).pipe(map((response: any) => {
            return response
        }))
    }

    // send startup detail
    doSendStartupDetail(data): Observable<any> {
        let url = "StartUp/api/StartUpDetail"
        let startupData = {
            "name": data.name,
            "activity": data.activity,
            "establishmentYear": data.establishmentYear,
            "aboutStartUp": data.aboutStartUp,
            "address": data.address,
            "telegram": data.telegram,
            "slogan": data.slogan
        }
        return this.service.post(url, startupData).pipe(map((response: any) => {
            return response
        }))
    }

    // send user detail
    doSendUserDetail(data): Observable<any> {
        let url = "User/api/UserDetail"
        let userData = {
            "fieldId": data.fieldId,
            "aboutMe": data.aboutMe,
            "universityId": data.universityId,
            "educationCityId": data.educationCityId,
            "educationDegreeId": data.educationDegreeId
        }
        return this.service.post(url, userData).pipe(map((response: any) => {
            return response
        }))
    }

    // upload profile image
    doUploadProfileImage(url, data) {
        let header = new HttpHeaders({
            'Accept': '*/*',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        });
        let formData: FormData = new FormData();
        formData.append("ProfileImage", data)

        return this.service.post(url, formData, { headers: header }).pipe(map((response: any) => {
            return response;
        }))
    }

    // uplaod user cover image
    doUploadCoverImage(url, data) {
        let header = new HttpHeaders({
            'Accept': '*/*',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        });
        let formData: FormData = new FormData();
        formData.append("CoverImage", data)

        return this.service.post(url, formData, { headers: header }).pipe(map((response: any) => {
            return response;
        }))
    }

    // upload resume file
    doUploadResumeFile(url, data) {
        let header = new HttpHeaders({
            'Accept': '*/*',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
        let formData: FormData = new FormData();
        formData.append("CV", data)

        return this.service.post(url, formData, { headers: header }).pipe(map((response: any) => {
            return response;
        }))
    }
}