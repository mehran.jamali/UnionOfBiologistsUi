import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';


import { DataService } from "../shared/services/data.service";
import { PublicService } from "../shared/services/public.service";

@Injectable()
export class SearchPageService {
    constructor(public dataService: DataService, public publicService: PublicService) { }

    // get articles 
    getArticles(data: any): Observable<any> {
        let url = 'api/Search/GetArticles';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // get professors
    getProfessors(data: any): Observable<any> {
        let url = 'api/Search/GetProfessors';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // get unions
    getUnions(data: any): Observable<any> {
        let url = 'api/Search/GetUnions';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

}