import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SearchPageComponent } from "./search-page.component";
import { SearchUnionComponent } from './search-union/search-union.component';
import { SearchProfessorComponent } from './search-professor/search-professor.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchPageService } from "./search-page.service";

@NgModule({
  declarations: [SearchPageComponent, SearchUnionComponent, SearchProfessorComponent, SearchArticleComponent],
  imports: [
    SharedModule.forRoot(),
  ],
  providers: [
    SearchPageService
  ]
})
export class SearchPageModule { }
