import { Component, SimpleChanges } from "@angular/core";
import { Router } from "@angular/router";
import { SharedService } from "../shared/shared.service";
import { SearchPageService } from "./search-page.service";
import { PublicService } from "../shared/services/public.service";
import * as AOS from "aos";

@Component({
    selector: 'search-result-app',
    templateUrl: './search-page.component.html',
    styleUrls: ['./search-page.component.scss'],
})

export class SearchPageComponent {
    // navbar data
    public checkLogin: boolean = false;
    public navbarMode: boolean = true;

    // search data
    public data: any;
    public typeId;
    public searchText;

    // items
    public items = [1]

    // page controller
    public pageNumber: number = 0;
    public pageSize: number = 0;

    // lists
    public unions = []
    public professors = []
    public contents = []
    public events = []

    // show loader
    public showLoader: boolean = true;
    public showListLoader: boolean = false;

    constructor(public sharedService: SharedService, public router: Router, public searchPageService: SearchPageService, public publicService: PublicService) { }

    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
    }

    ngOnInit(): void {
        AOS.init();
        this.checkToken()
        this.sharedService.searchData.subscribe(data => {
            this.data = data;
            this.doSearch();
        });
        if (window.performance.navigation.type == 1 && !this.data) {
            this.router.navigate(['/home'])
        }
        window.addEventListener('scroll', this.scroll, true); //third parameter

    }

    ngOnDestroy() {
        window.removeEventListener('scroll', this.scroll, true);
        console.clear()

    }


    scroll = (event): void => {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            this.doSearch()
        }
    };

    // Get Articles
    getArticles(typeId) {
        let body = {
            "typeId": typeId,
            "text": this.data.searchText,
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber
        }
        if (typeId == 1) {
            this.searchPageService.getArticles(body).subscribe(data => {
                if (this.data.categoryId == this.typeId && this.data.searchText == this.searchText) {
                    this.contents = [...data];
                }
                else {
                    this.contents = []
                    this.contents = data
                }
                setTimeout(() => {
                    this.typeId = this.data.categoryId;
                    this.searchText = this.data.searchText;
                    this.items = this.contents;
                }, 100);
            }, error => {
                if (this.data.categoryId !== this.typeId || this.data.searchText !== this.searchText) {
                    this.contents = []
                    this.items = [];
                }
                console.log('you have an error in "get articles (content)" ')
            })
        }
        else if (typeId == 2) {
            this.searchPageService.getArticles(body).subscribe(data => {
                if (this.data.categoryId == this.typeId && this.data.searchText == this.searchText) {
                    this.events = [...data];
                }
                else {
                    this.events = [];
                    this.events = data;
                }
                setTimeout(() => {
                    this.typeId = this.data.categoryId;
                    this.searchText = this.data.searchText;
                    this.items = this.events;
                }, 100);
            }, error => {
                if (this.data.categoryId !== this.typeId || this.data.searchText !== this.searchText) {
                    this.events = []
                    this.items = [];
                }
                console.log('you have an error in "get articles (events)" ')
            })
        }
    }

    // Get Professors
    getProfessors() {
        let body = {
            "typeId": this.data.categoryId,
            "text": this.data.searchText,
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber
        }
        this.searchPageService.getProfessors(body).subscribe(data => {
            if (this.data.categoryId == this.typeId && this.data.searchText == this.searchText) {
                this.professors = [...data];
            }
            else {
                this.professors = []
                this.professors = data;
            }
            setTimeout(() => {
                this.typeId = this.data.categoryId;
                this.searchText = this.data.searchText;
                this.items = this.professors;
            }, 100);
        }, error => {
            if (this.data.categoryId !== this.typeId || this.data.searchText !== this.searchText) {
                this.professors = []
                this.items = [];
            }
            console.log('you have an error in "get professors" ')
        })
    }

    // Get Unions
    getUnions() {
        let body = {
            "typeId": this.data.categoryId,
            "text": this.data.searchText,
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber
        }
        this.searchPageService.getUnions(body).subscribe(data => {
            if (this.data.categoryId == this.typeId && this.data.searchText == this.searchText) {
                this.unions = [...data];
            }
            else {
                this.unions = []
                this.unions = data;
            }
            setTimeout(() => {
                this.typeId = this.data.categoryId;
                this.searchText = this.data.searchText;
                this.items = this.unions;
            }, 100);
        }, error => {
            if (this.data.categoryId !== this.typeId || this.data.searchText !== this.searchText) {
                this.unions = []
                this.items = [];
            }
            console.log('you have an error in "get professors" ')
        })
    }

    doSearch() {
        if (this.data.categoryId == this.typeId && this.data.searchText == this.searchText) {
            if (this.data.categoryId == 1 || this.data.categoryId == 2) {
                this.pageNumber += 1;
                this.pageSize += 6;
            }
            else if (this.data.categoryId == 3 || this.data.categoryId == 4) {
                this.pageNumber += 1;
                this.pageSize += 8;
            }
        }
        else {
            if (this.data.categoryId == 1 || this.data.categoryId == 2) {
                this.pageNumber = 1;
                this.pageSize = 6;
            }
            else if (this.data.categoryId == 3 || this.data.categoryId == 4) {
                this.pageNumber = 1;
                this.pageSize = 8;
            }
        }

        if (this.data.categoryId == 1) {
            this.getArticles(1);
            // return "مطالب"
        }
        else if (this.data.categoryId == 2) {
            this.getArticles(2);
            // return "رویداد"
        }
        else if (this.data.categoryId == 3) {
            this.getUnions();
            // return "انجمن"
        }
        else if (this.data.categoryId == 4) {
            this.getProfessors()
            // return "استاد"
        }
        if (!this.showLoader) {
            this.showListLoader = true;
            setTimeout(() => {
                this.showListLoader = false;
            }, 500);
        }
        else {
            setTimeout(() => {
                this.showLoader = false;
                this.showListLoader = true;
            }, 500);
            setTimeout(() => {
                this.showListLoader = false;
            }, 1000);
        }
    }

    showCategory() {
        if (this.data.categoryId == 1) {
            return "مطالب"
        }
        else if (this.data.categoryId == 2) {
            return "رویداد"
        }
        else if (this.data.categoryId == 3) {
            return "انجمن"
        }
        else if (this.data.categoryId == 4) {
            return "استاد"
        }
    }

}
