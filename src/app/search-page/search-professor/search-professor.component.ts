import { Component, OnInit, Input } from '@angular/core';
import { PublicService } from "../../shared/services/public.service";

@Component({
  selector: 'app-search-professor',
  templateUrl: './search-professor.component.html',
  styleUrls: ['./search-professor.component.scss']
})
export class SearchProfessorComponent implements OnInit {

  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImage = '';

  @Input() data: any;

  constructor(public publicService: PublicService) { }

  ngOnInit() {
  }

}
