import { Component, OnInit, Input } from '@angular/core';
import { faComment, faHeart } from "@fortawesome/free-solid-svg-icons";
import { PublicService } from "../../shared/services/public.service";


@Component({
  selector: 'app-search-article',
  templateUrl: './search-article.component.html',
  styleUrls: ['./search-article.component.scss']
})
export class SearchArticleComponent implements OnInit {

  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImage = '';


  @Input() data: any;

  faComment = faComment;
  faHeart = faHeart;

  constructor(public publicService: PublicService) { }

  ngOnInit() {
  }

}
