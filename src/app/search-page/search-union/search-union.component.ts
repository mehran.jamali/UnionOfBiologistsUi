import { Component, OnInit, Input } from '@angular/core';
import { PublicService } from "../../shared/services/public.service";

@Component({
  selector: 'app-search-union',
  templateUrl: './search-union.component.html',
  styleUrls: ['./search-union.component.scss']
})
export class SearchUnionComponent implements OnInit {

  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImage = '';
  @Input() data: any;

  constructor(public publicService: PublicService) { }

  ngOnInit() {
  }

}
