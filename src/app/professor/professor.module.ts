// Modules
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
// Components
import { ProfessorComponent } from "./professor.component";
import { ProfessorItemComponent } from './professor-item/professor-item.component';
import { ProfessorDetailComponent } from './professor-detail/professor-detail.component';
import { DetailRecordsComponent } from './professor-detail/detail-records/detail-records.component';
import { DetailContentComponent } from './professor-detail/detail-content/detail-content.component';
import { DetailProgramComponent } from './professor-detail/detail-program/detail-program.component';
import { DetailArticleComponent } from './professor-detail/detail-article/detail-article.component';
import { DetailNotificationComponent } from './professor-detail/detail-notification/detail-notification.component';
import { DetailActivitiesComponent } from './professor-detail/detail-activities/detail-activities.component';
import { ContentItemComponent } from './professor-detail/detail-content/content-item/content-item.component';
import { ArticleItemComponent } from './professor-detail/detail-article/article-item/article-item.component';
import { NotificationItemComponent } from "./professor-detail/detail-notification/notification-item/notification-item.component";
// Services
import { ProfessorService } from "./professor.service";

@NgModule({
  declarations: [
    ProfessorComponent,
    ProfessorItemComponent,
    ProfessorDetailComponent,
    DetailRecordsComponent,
    DetailContentComponent,
    DetailProgramComponent,
    DetailArticleComponent,
    DetailNotificationComponent,
    DetailActivitiesComponent,
    ContentItemComponent,
    ArticleItemComponent,
    NotificationItemComponent
  ],
  imports: [
    SharedModule.forRoot(),
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [
    ProfessorService
  ]
})
export class ProfessorModule { }
