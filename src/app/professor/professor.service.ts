import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';


import { DataService } from "../shared/services/data.service";
import { PublicService } from "../shared/services/public.service";

@Injectable()
export class ProfessorService {
    constructor(public dataService: DataService, public publicService: PublicService) { }


    // get professor list
    getProfessorList(data): Observable<any> {
        let url = "api/List/GetProfessors";
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get professor detail
    getProfessorDetail(id): Observable<any> {
        let url = `api/ProfessorProfile/GetProfessorInfo/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // professor profile is followed
    professorIsFollowed(id): Observable<any> {
        let url = `api/ProfessorProfile/IsFollowed/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // follow
    follow(id): Observable<any> {
        let url = `api/Follow/FollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // unfollow
    unFollow(id): Observable<any> {
        let url = `api/Follow/UnFollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get academic backgrounds
    getAcademicBackgrounds(id): Observable<any> {
        let url = `api/ProfessorProfile/GetAcademicBackgrounds/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get recruitment information
    getRecruitmentInformation(id): Observable<any> {
        let url = `api/ProfessorProfile/GetRecruitmentInformation/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get record
    getRecord(id): Observable<any> {
        let url = `api/ProfessorProfile/GetRecord/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get content 
    getContent(data): Observable<any> {
        let url = "api/ProfessorProfile/GetContents"
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get weekly schedule
    getWeeklySchedule(id): Observable<any> {
        let url = `api/ProfessorProfile/GetWeeklyScheduleWeb/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get articles
    getArticles(data): Observable<any> {
        let url = 'api/ProfessorProfile/GetArticles';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get notification
    getNotifications(data): Observable<any> {
        let url = "api/ProfessorProfile/GetNotifications";
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get activities
    getActivities(id): Observable<any> {
        let url = `api/ProfessorProfile/GetActivityWeb/${id}`
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }
}