import { Component } from "@angular/core";
import * as AOS from "aos";
import * as $ from "jquery";

// icons
import { faAngleRight, faAngleLeft, faSearch } from "@fortawesome/free-solid-svg-icons";

// forms
import { FormGroup, FormControl, Validators } from "@angular/forms"

// Service
import { PublicService } from "../shared/services/public.service";
import { SharedService } from "../shared/shared.service";
import { ProfessorService } from "./professor.service";


@Component({
    selector: 'professor-app',
    templateUrl: './professor.component.html',
    styleUrls: ['./professor.component.scss']
})

export class ProfessorComponent {
    // navbar
    public checkLogin: boolean = false;
    public navbarMode: boolean = true;

    // icons
    faAngleRight = faAngleRight;
    faAngleLeft = faAngleLeft;
    faSearch = faSearch;

    // page controller
    public pageNumber = 1;
    public pageSize = 4;
    setDefaultPage() {
        this.pageNumber = 1;
        this.pageSize = 4;
    }

    // show loader
    public showLoader = true;
    public showListLoader = false;

    // selected values
    public selectValues = {
        "cityId": 0,
        "fieldId": 0,
        "educationDegreeId": 0,
        "academicRank": 0,
    }

    // form
    public filterForm = new FormGroup({
        search: new FormControl("", []),
        province: new FormControl("", []),
        city: new FormControl({ value: '', disabled: true }, []),
        degree: new FormControl("", []),
        field: new FormControl("", []),
        level: new FormControl("", [])
    })


    // list 
    public professors = []
    public provinces = []
    public cities = []
    public degrees = []
    public fields = []
    public levels = []


    public filteredProvinces;
    public filteredCities;
    public filteredDegrees;
    public filteredFields;
    public filteredLevels = this.levels;

    constructor(public sharedService: SharedService, public publicService: PublicService, public professorService: ProfessorService) { }

    // go previous
    goPrevious() {
        if (this.pageNumber > 1) {
            this.pageNumber -= 1;
            this.pageSize = 4;
            this.doFilter()
        }
        else {
            // do nothing
        }
    }

    // go next
    goNext() {
        if (this.professors.length < 4) {
            if (this.professors.length == 0) {
                this.pageNumber -= 1;
                this.pageSize = 4;
                this.doFilter()
            }
        }
        else {
            this.pageNumber += 1;
            this.pageSize = 4;
            this.doFilter()
        }
    }

    // get province
    getProvince() {
        this.publicService.getProvince().subscribe(data => {
            this.provinces = data;
            this.filteredProvinces = this.provinces;
        }, error => {
            // do nothing
        })
    }

    // get city by province id
    getCityByProvinceId(id) {
        this.publicService.getCityByProvinceId(id).subscribe(data => {
            this.cities = data;
            this.filteredCities = this.cities;
        }, error => {
            // do nothing

        })
    }

    // get fields 
    getFields() {
        this.publicService.getFields().subscribe(data => {
            this.fields = data;
            this.filteredFields = this.fields
        }, error => {
            // do nothing
        })
    }

    // get degree
    getDegrees() {
        this.publicService.getEducationDegree().subscribe(data => {
            this.degrees = data;
            this.filteredDegrees = this.degrees;
        }, error => {
            // do nothing
        })
    }

    // do filter 
    doFilter() {
        let body = {
            "cityId": this.selectValues.cityId,
            "fieldId": this.selectValues.fieldId,
            "educationDegreeId": this.selectValues.educationDegreeId,
            "academicRank": this.selectValues.academicRank,
            "searchText": this.filterForm.value.search != "" ? this.filterForm.value.search : "",
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber,
        }
        console.log(body);
        this.professors = [];
        this.showListLoader = true;

        this.professorService.getProfessorList(body).subscribe(data => {
            this.professors = data;
            if (this.showLoader) {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 1500);
            }
            else {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 500);
            }

            setTimeout(() => {
                this.showLoader = false;
            }, 1000);
        }, error => {
            setTimeout(() => {
                this.showListLoader = false;
            }, 500);
        })
    }

    // check token
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
    }


    ngOnInit(): void {
        this.checkToken();
        this.doFilter();
        this.getProvince();
        this.getFields();
        this.getDegrees()

        AOS.init();
        $(document).ready(function () {
            $("#tab-box div").click(function () {
                $("#tab-box div").removeClass('active');
                $(this).addClass("active");
            })
            // open dropdown
            $(".select-input").focus(function () {
                $("#" + $(this).attr('name')).addClass("show-dropdown");
            })
            // close dropdown
            $(".select-input").focusout(function () {
                $("#" + $(this).attr('name')).removeClass("show-dropdown");
            })
        })
    }

    // select 
    // select province 
    setProvinceValue(value) {
        this.filterForm.controls.province.setValue(value.name);
        this.filterForm.controls['city'].enable();
        this.filteredProvinces = this.provinces;
        this.getCityByProvinceId(value.id);
        this.setDefaultPage()
    }

    // select city
    setCityValue(value) {
        this.filterForm.controls.city.setValue(value.name);
        this.selectValues.cityId = value.id;
        this.filteredCities = this.cities;
        this.setDefaultPage()
    }

    // select degree
    setDegreeValue(value) {
        this.filterForm.controls.degree.setValue(value.name);
        this.selectValues.educationDegreeId = value.id;
        this.filteredDegrees = this.degrees;
        this.setDefaultPage()
    }

    // select level
    setLevelValue(value) {
        this.filterForm.controls.level.setValue(value.name);
        this.selectValues.academicRank = value.id;
        this.filteredLevels = this.levels;
        this.setDefaultPage()
    }

    // select field
    setFieldValue(value) {
        this.filterForm.controls.field.setValue(value.name);
        this.selectValues.fieldId = value.id;
        this.filteredFields = this.fields;
        this.setDefaultPage()
    }

    // filter 
    // filter province
    filterProvince() {
        this.filteredProvinces = this.provinces.filter(x => {
            return x.name.includes(this.filterForm.value.province);
        })
    }

    // filter city
    filterCity() {
        this.filteredCities = this.cities.filter(x => {
            return x.name.includes(this.filterForm.value.city);
        })
    }

    // filter level
    filterLevel() {
        this.filteredLevels = this.levels.filter(x => {
            return x.name.includes(this.filterForm.value.field);
        })
    }

    // filter degree
    filterDegree() {
        this.filteredDegrees = this.degrees.filter(x => {
            return x.name.includes(this.filterForm.value.degree);
        })
    }

    // filter field
    filterField() {
        this.filteredFields = this.fields.filter(x => {
            return x.name.includes(this.filterForm.value.field);
        })
    }

} 