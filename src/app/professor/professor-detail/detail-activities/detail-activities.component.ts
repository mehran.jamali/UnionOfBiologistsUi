import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import { ActivatedRoute } from "@angular/router";

// Service
import { ProfessorService } from "../../professor.service";

@Component({
  selector: 'app-detail-activities',
  templateUrl: './detail-activities.component.html',
  styleUrls: ['./detail-activities.component.scss']
})
export class DetailActivitiesComponent implements OnInit {

  // loader
  public showContentLoader = false;

  // data
  public data: any = {
    'description': ''
  }

  // id
  public professorId = '';

  constructor(public professorService: ProfessorService, public activatedRoute: ActivatedRoute) { }

  // get activities
  getActivities() {
    this.professorService.getActivities(this.professorId).subscribe(data => {
      this.data = data;
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => { })
  }

  ngOnInit() {
    this.professorId = this.activatedRoute.snapshot.params['id'];
    this.getActivities();
    AOS.init()
  }

}
