import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from "@angular/router";
// Service
import { ProfessorService } from "../../professor.service";
import { PublicService } from "../../../shared/services/public.service";

@Component({
  selector: 'app-detail-program',
  templateUrl: './detail-program.component.html',
  styleUrls: ['./detail-program.component.scss']
})
export class DetailProgramComponent implements OnInit {

  // loader
  public showContentLoader: boolean = false;

  // id
  public professorId: string = ''

  // image
  public data: any = { 'image': '' };
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImageUrl = 'DefaultImages/weeklySchedule.jpg';

  // icons
  faDownload = faDownload;

  constructor(public professorService: ProfessorService, public activatedRoute: ActivatedRoute, public publicService: PublicService) { }

  // get weekly schedule
  getWeeklySchedule() {
    this.professorService.getWeeklySchedule(this.professorId).subscribe(data => {
      this.data = data;
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => { })
  }

  ngOnInit() {
    this.professorId = this.activatedRoute.snapshot.params['id']
    this.getWeeklySchedule()

    AOS.init();
  }

}
