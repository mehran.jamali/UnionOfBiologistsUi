import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";

import { PublicService } from "../../../../shared/services/public.service";

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss']
})
export class ArticleItemComponent implements OnInit {
  @Input() data: any;

  // image
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImageUrl = 'DefaultImages/article.jpg';


  constructor(public publicService: PublicService) { }

  ngOnInit() {
    AOS.init()
  }

}
