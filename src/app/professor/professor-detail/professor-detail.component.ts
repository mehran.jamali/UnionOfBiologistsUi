import { Component, OnInit } from '@angular/core';
import { faPlus, faDownload, faTimes } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { ActivatedRoute } from "@angular/router"

// services
import { ProfessorService } from "../professor.service";
import { PublicService } from "../../shared/services/public.service";


@Component({
  selector: 'app-professor-detail',
  templateUrl: './professor-detail.component.html',
  styleUrls: ['./professor-detail.component.scss']
})
export class ProfessorDetailComponent implements OnInit {

  // navbar
  public isLogin: boolean = false;
  public navbarMode: boolean = true;

  // urls
  public imageUrl = this.publicService.urls.imageUrl
  public cvUrl = this.publicService.urls.cvUrl
  public defaultImageUrl = 'DefaultImages/professor.jpg'

  // user id
  public userId: string = '';

  // link text
  public linkText: string = '';
  public showLinkTextBox: boolean = false;

  // professor
  public professorId: string = '';
  public professorDetail = {
    "fullName": "string",
    "field": "string",
    "academicRank": "string",
    "educationDegree": "string",
    "collegeName": "string",
    "birthYear": 0,
    "profileImage": "string",
    "linkedIn": "string",
    "twitter": "string",
    "facebook": "string",
    "instagram": "string",
    "telegram": "string",
    "cv": "string",
    "email": "string",
    "phoneNumber": "string"
  }

  // check follow
  public isFollowed: boolean = false;

  // tab name
  public tabName: string = '';

  //icons
  faDownload = faDownload;
  faPlus = faPlus;
  faTimes = faTimes;

  // show loader
  public showLoader = true;
  public showContentLoader = false;

  changeTab(value: any) {
    console.log(value);
    this.tabName = value;
    this.showContentLoader = true;
    setTimeout(() => {
      this.showContentLoader = false;
    }, 500);
  }

  showLinkText(value) {
    switch (value) {
      case 1:
        this.linkText = this.professorDetail.linkedIn;
        break;
      case 2:
        this.linkText = this.professorDetail.telegram;
        break;
      case 3:
        this.linkText = this.professorDetail.twitter;
        break;
      case 4:
        this.linkText = this.professorDetail.facebook;
        break;
      case 5:
        this.linkText = this.professorDetail.instagram;
        break;

      default:
        break;
    }
    this.showLinkTextBox = true
    setTimeout(() => {
      this.showLinkTextBox = false
    }, 3000);
  }

  constructor(public professorService: ProfessorService, public publicService: PublicService, public activatedRoute: ActivatedRoute) { }

  // check token
  checkToken() {
    this.publicService.checkToken().subscribe(data => {
      this.isLogin = true;
    }, error => {
      this.isLogin = false;
      console.clear()
    })
  }

  // check follow
  checkFollow() {
    this.professorService.professorIsFollowed(this.professorId).subscribe(data => {
      this.isFollowed = data;
    }, error => {
      console.clear()
    })
  }

  // follow
  follow() {
    this.professorService.follow(this.professorId).subscribe(data => {
      this.isFollowed = true
    }, error => {
      console.clear()
    })
  }

  // un follow
  unFollow() {
    this.professorService.unFollow(this.professorId).subscribe(data => {
      this.isFollowed = false
    }, error => {
      console.clear()
    })
  }



  // get professor detail
  getProfessorDetail() {
    this.professorService.getProfessorDetail(this.professorId).subscribe(data => {
      this.professorDetail = data;
      setTimeout(() => {
        this.showLoader = false;
        this.showContentLoader = true;
        this.tabName = 'records';
        setTimeout(() => {
          this.showContentLoader = false;
        }, 500);
      }, 1000);
    }, error => {
      console.clear()
    })
  }



  ngOnInit() {
    this.userId = localStorage.getItem('id')
    this.checkToken()

    this.professorId = this.activatedRoute.snapshot.params['id']
    if (this.professorId) {
      this.checkFollow();
      this.getProfessorDetail();
    }

    AOS.init();
    $(document).ready(function () {
      $('.tab').click(function () {
        // remove active 
        $('.tab').removeClass('active');
        $('.content-box div').removeClass('content-active');

        // add active 
        $(this).addClass('active');
        $("#" + $(this).attr('role')).addClass('content-active');
      })
    })
  }

}
