import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { faAngleRight, faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { ActivatedRoute } from "@angular/router";

// Service
import { ProfessorService } from "../../professor.service";
import { PublicService } from "../../../shared/services/public.service";

@Component({
  selector: 'app-detail-event',
  templateUrl: './detail-notification.component.html',
  styleUrls: ['./detail-notification.component.scss']
})
export class DetailNotificationComponent implements OnInit {

  // icons
  faAngleRight = faAngleRight;
  faAngleLeft = faAngleLeft;

  // professor id
  public professorId: string = '';

  // page 
  public pageNumber = 0;
  public pageSize = 0;

  // loader
  public showContentLoader = false;

  // list
  public items: any = []

  constructor(public professorService: ProfessorService, public publicService: PublicService, public activatedRoute: ActivatedRoute) { }

  // get notifications
  getNotification() {
    this.pageNumber += 1;
    this.pageSize = 4;
    let data = {
      "professorId": this.professorId,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }

    this.professorService.getNotifications(data).subscribe(data => {
      this.items.push(...data)
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => {
      console.clear()
    })

  }

  ngOnInit() {
    this.professorId = this.activatedRoute.snapshot.params['id'];
    this.getNotification()
    AOS.init();
    window.addEventListener('scroll', this.scroll, true)
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true)
  }

  scroll = (event): void => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getNotification()
    }
  }

}
