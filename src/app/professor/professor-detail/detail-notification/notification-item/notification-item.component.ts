import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";
import { PublicService } from "../../../../shared/services/public.service";

@Component({
    selector: 'app-notification-item',
    templateUrl: './notification-item.component.html',
    styleUrls: ['./notification-item.component.scss']
})
export class NotificationItemComponent implements OnInit {
    // data
    @Input() data: any;

    // image
    public imageUrl = this.publicService.urls.imageUrl;
    public defaultImageUrl = 'DefaultImages/article.jpg';

    constructor(public publicService: PublicService) { }

    ngOnInit() {
        AOS.init()
    }

}
