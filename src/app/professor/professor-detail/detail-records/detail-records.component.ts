import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import { ProfessorService } from "../../professor.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-detail-records',
  templateUrl: './detail-records.component.html',
  styleUrls: ['./detail-records.component.scss']
})
export class DetailRecordsComponent implements OnInit {

  public professorId: string = '';

  public record: string = "";

  public academicBackgrounds: any = []

  public recruitmentInformations: any = []

  public showContentLoader: boolean = false;

  constructor(public professorService: ProfessorService, public activatedRoute: ActivatedRoute) { }

  // academic backgrounds
  getAcademicBackgrounds() {
    this.professorService.getAcademicBackgrounds(this.professorId).subscribe(data => {
      this.academicBackgrounds = data;
    }, error => { })
  }

  // get recruitment information
  getRecruitmentInformation() {
    this.professorService.getRecruitmentInformation(this.professorId).subscribe(data => {
      this.recruitmentInformations = data;
    }, error => { })
  }

  // get record
  getRecord() {
    this.professorService.getRecord(this.professorId).subscribe(data => {
      this.record = data.description
    }, error => { })
    this.showContentLoader = true;
    setTimeout(() => {
      this.showContentLoader = false;
    }, 1000);
  }

  ngOnInit() {
    this.professorId = this.activatedRoute.snapshot.params['id'];
    if (this.professorId) {
      this.getAcademicBackgrounds()
      this.getRecruitmentInformation()
      this.getRecord()
    }

    AOS.init();
  }

  ngOnDestroy(): void {
    console.clear()
  }

}
