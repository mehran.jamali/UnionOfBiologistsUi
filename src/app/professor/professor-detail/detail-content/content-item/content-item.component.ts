import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";
import { PublicService } from "../../../../shared/services/public.service";

@Component({
  selector: 'app-content-item',
  templateUrl: './content-item.component.html',
  styleUrls: ['./content-item.component.scss']
})
export class ContentItemComponent implements OnInit {
  // data
  @Input() data: any;

  // image
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImageUrl = 'DefaultImages/article.jpg';

  constructor(public publicService: PublicService) { }

  ngOnInit() {
    AOS.init()
  }

}
