import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { faAngleRight, faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { ActivatedRoute } from "@angular/router";
import { ProfessorService } from "../../professor.service";

@Component({
  selector: 'app-detail-content',
  templateUrl: './detail-content.component.html',
  styleUrls: ['./detail-content.component.scss']
})
export class DetailContentComponent implements OnInit {

  // loader
  public showContentLoader: boolean = false;

  // id
  public professorId: string = ''

  // icons 
  faAngleRight = faAngleRight;
  faAngleLeft = faAngleLeft;

  // list
  public items: any = []

  // page 
  public pageNumber = 0;
  public pageSize = 0;

  constructor(public activatedRoute: ActivatedRoute, public professorService: ProfessorService) { }

  // get content
  getContent() {
    this.pageNumber += 1;
    this.pageSize = 4;
    let data = {
      "professorId": this.professorId,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }
    this.professorService.getContent(data).subscribe(data => {
      this.items.push(...data);
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    this.professorId = this.activatedRoute.snapshot.params['id'];
    this.getContent()
    AOS.init();
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (event): void => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getContent()
    }
  }

}
