import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";
import { PublicService } from "../../shared/services/public.service";

@Component({
  selector: 'app-professor-item',
  templateUrl: './professor-item.component.html',
  styleUrls: ['./professor-item.component.scss']
})
export class ProfessorItemComponent implements OnInit {
  // data
  @Input() data: any;

  // image
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImageUrl = 'DefaultImages/professor.jpg';

  constructor(public publicService: PublicService) { }

  ngOnInit() {
    AOS.init()
  }

}
