// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HomeModule } from "./home/home.module";
import { SearchPageModule } from "./search-page/search-page.module";
import { SharedModule } from './shared/shared.module';
import { ThemeModule } from "./@theme/theme.module";
import { AppRoutingModule } from './app-routing.module';
import { CommunityModule } from "./community/community.module";
import { ProfessorModule } from "./professor/professor.module";
import { StartupModule } from "./startup/startup.module";
import { ArticlesModule } from './articles/articles.module';
import { AboutUsModule } from "./about-us/about-us.module";
import { ContactUsModule } from "./contact-us/contact-us.module";
import { UserProfileModule } from "./user-profile/user-profile.module";
import { ArticlePostModule } from "./article-post/article-post.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DashboardModule } from './dashboard/dashboard.module';
import { LoginRegisterModule } from "../app/login-register/login-register.module";
import { HttpClientModule } from "@angular/common/http";

// Service
import { DataService } from "./shared/services/data.service"

// Components
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    HomeModule,
    SearchPageModule,
    ThemeModule,
    CommunityModule,
    ProfessorModule,
    StartupModule,
    ArticlesModule,
    AboutUsModule,
    ContactUsModule,
    UserProfileModule,
    ArticlePostModule,
    DashboardModule,
    LoginRegisterModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent],
})
export class AppModule { }
