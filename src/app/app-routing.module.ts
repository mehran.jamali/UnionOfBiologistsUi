// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component
import { HomeComponent } from "./home/home.component";
import { SearchPageComponent } from "./search-page/search-page.component";
import { CommunityComponent } from "./community/community.component";
import { CommunityDetailComponent } from './community/community-detail/community-detail.component';
import { ProfessorComponent } from "./professor/professor.component";
import { ProfessorDetailComponent } from "./professor/professor-detail/professor-detail.component";
import { StartupComponent } from "./startup/startup.component";
import { StartupDetailComponent } from "./startup/startup-detail/startup-detail.component";
import { ArticlesComponent } from "./articles/articles.component";
import { AboutUsComponent } from "./about-us/about-us.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { OtherPostComponent } from "./article-post/other-post/other-post.component";
import { NotificationPostComponent } from "./article-post/notification-post/notification-post.component"
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "../app/login-register/login/login.component";
import { RegisterComponent } from "../app/login-register/register/register.component";

const routes: Routes = [
  { path: '', redirectTo: "home", pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'search', component: SearchPageComponent },
  { path: 'union', component: CommunityComponent },
  { path: 'union/:id', component: CommunityDetailComponent },
  { path: 'professor', component: ProfessorComponent },
  { path: 'professor/:id', component: ProfessorDetailComponent },
  { path: 'startup', component: StartupComponent },
  { path: 'startup/:id', component: StartupDetailComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'user-profile/:id', component: UserProfileComponent },
  { path: 'article/:id', component: OtherPostComponent },
  { path: 'notification/:id', component: NotificationPostComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
