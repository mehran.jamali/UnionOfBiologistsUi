import { Component } from "@angular/core";

// service
import { AboutUsService } from "./about-us.service";
import { PublicService } from "../shared/services/public.service";


@Component({
    selector: 'app-about-us',
    templateUrl: './about-us.component.html',
    styleUrls: ['./about-us.component.scss']
})

export class AboutUsComponent {
    public checkLogin: boolean;
    public navbarMode: boolean = false;

    // show loader
    public showLoader = true;

    constructor(public publicService: PublicService) { }

    // check token
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
        setTimeout(() => {
            this.showLoader = false;
        }, 1000);
    }


    ngOnInit(): void {
        this.checkToken();

    }

}