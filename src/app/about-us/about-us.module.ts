// Module
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";

// Component
import { AboutUsComponent } from "./about-us.component";

// Service
import { AboutUsService } from "./about-us.service";

@NgModule({
  declarations: [
    AboutUsComponent
  ],
  imports: [
    SharedModule.forRoot()
  ],
  providers: [
    AboutUsService
  ]
})
export class AboutUsModule { }
