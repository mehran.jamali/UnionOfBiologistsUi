import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import { PublicService } from "../../shared/services/public.service";

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss']
})
export class ArticleItemComponent implements OnInit {
  @Input() data: any;
  @Input() postType: any;

  // image
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultUserImage = "DefaultImages/user.jpg";
  public defaultArticleImage = "DefaultImages/article.jpg";

  response = {
    "id": "string",
    "title": "string",
    "image": "string",
    "userProfileImage": "string"
  }


  constructor(public router: Router, public publicService: PublicService) { }

  ngOnInit() {
  }

  changeTab() {
    if (this.postType == 1) {
      let url = "/notification/" + this.data.id;
      this.router.navigate([url]);
    }
    else {
      let url = "/article/" + this.data.id;
      this.router.navigate([url]);
    }
  }

}
