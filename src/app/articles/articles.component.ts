import { Component } from "@angular/core";
import * as $ from "jquery";
import { faAngleRight, faAngleLeft, faSearch } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";

// service
import { ArticlesService } from "./article.service";
import { PublicService } from "../shared/services/public.service";

import { FormGroup, FormControl } from "@angular/forms"

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent {
    // navbar
    public checkLogin: boolean = false;
    public navbarMode: boolean = true;

    // page
    public pageNumber = 1;
    public pageSize = 6;
    setDefaultPage() {
        this.pageNumber = 1;
        this.pageSize = 6;
        console.log("page number: ", this.pageNumber, " page size: ", this.pageSize);
    }

    // show loader
    public showLoader = true;
    public showListLoader = false;

    // selected values
    public selectValues =
        {
            "role": 0,
            "type": 0,
            "groupId": 0,
            "postType": 0
        }

    // tab
    public currentTab: any;

    // post type
    public postType: any = 0;

    // icons
    faAngleRight = faAngleRight;
    faAngleLeft = faAngleLeft;
    faSearch = faSearch;

    // form
    public filterForm = new FormGroup({
        search: new FormControl("", []),
        role: new FormControl("", []),
        type: new FormControl("", []),
        postType: new FormControl("", []),
        group: new FormControl("", []),
    })

    // lists
    public items = []

    public roles = [
        { "id": 1, "name": 'انجمن علمی' },
        { "id": 2, "name": 'انجمن فرهنگی هنری' },
        { "id": 3, "name": 'استارتاپ' },
        { "id": 4, "name": 'استاد' },
    ]

    public types = [
        { "id": 1, "name": 'جدیدترین' },
        { "id": 2, "name": 'پر امتیاز ترین' },
        { "id": 3, "name": 'پر بحث ترین' },
        { "id": 4, "name": 'پربازدید ترین' },
    ]

    public postTypes = [
        { "id": 1, "title": " مقاله " },
        { "id": 2, "title": " رویداد " },
        { "id": 3, "title": " اخبار " },
        { "id": 7, "title": " اطلاعیه " },
    ];

    public groups = []
    public filteredGroups;

    constructor(public articlesService: ArticlesService, public publicService: PublicService) { }

    // go previous
    goPrevious() {
        if (this.pageNumber > 1) {
            this.pageNumber -= 1;
            this.pageSize = 6;
            this.doFilter()
        }
        else {
            // do nothing
        }
    }

    // go next
    goNext() {
        if (this.items.length < 6) {
            if (this.items.length == 0) {
                this.pageNumber -= 1;
                this.pageSize = 6;
                this.doFilter()
            }
        }
        else {
            this.pageNumber += 1;
            this.pageSize = 6;
            this.doFilter()
        }
    }

    // do filter 
    doFilter() {
        let body = {
            "searchText": this.filterForm.value.search != "" ? this.filterForm.value.search : "",
            "role": this.selectValues.role,
            "type": this.selectValues.type,
            "postType": this.selectValues.postType,
            "groupId": this.selectValues.groupId,
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber
        }

        console.log(body);

        this.items = [];
        this.showListLoader = true;

        this.articlesService.getArticles(body).subscribe(data => {
            this.items = data;
            if (this.showLoader) {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 1500);
            }
            else {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 500);
            }
            setTimeout(() => {
                this.showLoader = false;
            }, 1000);

        }, error => {
            setTimeout(() => {
                this.showListLoader = false;
            }, 500);
        })
    }

    // check token
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
    }

    // get group list
    getGroups() {
        this.articlesService.getGroups().subscribe(data => {
            this.groups = data;
            this.filteredGroups = this.groups;
        }, error => {
            // do nothing
        })
    }



    ngOnInit(): void {

        this.checkToken();
        this.getGroups()
        this.doFilter();

        AOS.init();
        $(document).ready(function () {
            $("#tab-box div").click(function () {
                $("#tab-box div").removeClass('active');
                $(this).addClass("active");
            })
            // open dropdown
            $(".select-input").focus(function () {
                $("#" + $(this).attr('name')).addClass("show-dropdown");
            })
            // close dropdown
            $(".select-input").focusout(function () {
                $("#" + $(this).attr('name')).removeClass("show-dropdown");
            })
        })
    }

    roleSelect(e: any) {
        if (e.srcElement.value == 2) {
            this.postTypes = [
                { "id": 4, "title": " عکس " },
                { "id": 5, "title": " ویدیو " },
                { "id": 6, "title": " پادکست " },
                { "id": 7, "title": " اطلاعیه " },
            ];
        }
        else {
            this.postTypes = [
                { "id": 1, "title": " مقاله " },
                { "id": 2, "title": " رویداد " },
                { "id": 3, "title": " اخبار " },
                { "id": 7, "title": " اطلاعیه " },
            ];

        }
    }

    // filter group
    filterGroup() {
        this.filteredGroups = this.groups.filter(x => {
            return x.title.includes(this.filterForm.value.group);
        })
    }

    // set post type value
    setPostTypeValue(value) {
        if (value.id == 7) {
            this.postType = 1;
        }
        else {
            this.postType = 0;
        }
        this.filterForm.controls.postType.setValue(value.title);
        this.selectValues.postType = value.id;
        this.setDefaultPage()
    }

    // set role value
    setRoleValue(value) {
        this.filterForm.controls.role.setValue(value.name);
        this.selectValues.role = value.id;
        this.setDefaultPage()
    }

    // set group value 
    setGroupValue(value) {
        this.filterForm.controls.group.setValue(value.title);
        this.filteredGroups = this.groups;
        this.selectValues.groupId = value.id;
        this.setDefaultPage()
    }

    // set type value
    setTypeValue(value) {
        this.filterForm.controls.type.setValue(value.name);
        this.selectValues.type = value.id;
        this.setDefaultPage()
    }

}
