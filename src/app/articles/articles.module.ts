// Modules
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";

// Components
import { ArticlesComponent } from "./articles.component";
import { ArticleItemComponent } from './article-item/article-item.component';

// service
import { ArticlesService } from "./article.service";

@NgModule({
  declarations: [
    ArticleItemComponent,
    ArticlesComponent
  ],
  imports: [
    SharedModule.forRoot()
  ],
  providers: [
    ArticlesService
  ]
})
export class ArticlesModule { }
