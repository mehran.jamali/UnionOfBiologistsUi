import { Injectable } from "@angular/core";
import { DataService } from "../shared/services/data.service";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';

@Injectable()
export class ArticlesService {
    constructor(public dataService: DataService) { }

    // get articles
    getArticles(data): Observable<any> {
        let url = "api/List/GetArticles";
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // get group 
    getGroups(): Observable<any> {
        let url = 'Admin/api/Group';
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }
}