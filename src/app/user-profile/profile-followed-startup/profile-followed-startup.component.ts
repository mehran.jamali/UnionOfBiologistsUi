import { Component, OnInit } from '@angular/core';
import { UserProfileService } from "../user-profile.service";
import { PublicService } from "../../shared/services/public.service";
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-profile-followed-startup',
  templateUrl: './profile-followed-startup.component.html',
  styleUrls: ['./profile-followed-startup.component.scss']
})
export class ProfileFollowedStartupComponent implements OnInit {

  // loader
  public showLoader = true;

  // data
  public postType: any = 2;
  public items: any = []

  // id
  public id = ''

  // page
  public pageNumber = 0;
  public pageSize = 0;

  // get followed startup 
  getFollowedStartUp() {
    this.pageNumber += 1;
    this.pageSize = 8;
    let data = {
      "userId": this.id,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }
    if (this.items.length >= 8) {
      this.userProfileService.getFollowedStartups(data).subscribe(data => {
        this.items.push(...data);
        this.showLoader = true;
        setTimeout(() => {
          this.showLoader = false;
        }, 500);

      }, error => {
        console.clear()
      })
    }
  }

  constructor(public publicService: PublicService, public userProfileService: UserProfileService, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    setTimeout(() => {
      this.showLoader = false;
    }, 500);
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getFollowedStartUp()
    window.addEventListener('scroll', this.scroll, true); //third parameter

  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }


  scroll = (event): void => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getFollowedStartUp()
    }
  };

}
