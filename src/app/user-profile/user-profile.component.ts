import { Component } from "@angular/core";
import * as AOS from "aos";
import * as $ from "jquery";
import { PublicService } from "../shared/services/public.service";
import { UserProfileService } from "./user-profile.service";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "app-user-profile",
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent {

    // navbar data
    public isLogin: boolean = true;
    public navbarMode: boolean = true;

    // imageUrl
    public imageUrl = this.publicService.urls.imageUrl;
    public defaultCoverImage = ''
    public defaultProfileImage = ''

    // id
    public id = ''

    // show tab value
    public tabValue: string = '';

    // user profile data
    public userProfileFullData: any = {
        "name": "",
        "family": "",
        "city": "",
        "field": "",
        "educationCity": "",
        "university": "",
        "aboutMe": ""
    };
    public userProfileData: any = {
        "name": "",
        "educationDegree": "",
        "profileImage": "",
        "coverImage": ""
    };

    // loader
    public showLoader: boolean = true;

    constructor(public publicService: PublicService, public userProfileService: UserProfileService, public activatedRoute: ActivatedRoute) { }

    // change tab
    changeTab(value) {
        this.tabValue = value;
    }

    // check token 
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.isLogin = true;
        }, error => {
            this.isLogin = false;
        })
    }

    // get user full info 
    getUserFullInfo() {
        this.userProfileService.getUserFullInfo(this.id).subscribe(data => {
            this.userProfileFullData = data;
            console.log(data);
        }, error => {
            console.log('you have an error in user profile "get user full info" ');
            // do nothing
        })
    }

    // get user info
    getUserInfo() {
        this.userProfileService.getUserInfo(this.id).subscribe(data => {
            this.userProfileData = data;
            console.log(data);
            setTimeout(() => {
                this.showLoader = false;
                this.tabValue = 'userData';
            }, 1000);
        }, error => {
            console.log('you have an error in user profile "get user info" ');
            // do nothing
        })
    }

    ngOnInit(): void {
        this.id = this.activatedRoute.snapshot.params['id']
        if (this.id) {
            this.getUserInfo();
            this.getUserFullInfo();
        }
        this.checkToken();
        AOS.init();

        $(document).ready(function () {
            $(".tab p").click(function () {
                $('.tab p').removeClass('active');
                $(this).addClass('active');
            })
        })

    }
}