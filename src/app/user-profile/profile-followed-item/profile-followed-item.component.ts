import { Component, Input } from '@angular/core';
import { Router } from "@angular/router";
import { PublicService } from "../../shared/services/public.service";

@Component({
    selector: 'app-profile-followed-item',
    templateUrl: './profile-followed-item.component.html',
    styleUrls: ['./profile-followed-item.component.scss']
})
export class ProfileFollowedItemComponent {
    @Input() data: any;
    @Input() postType: any;

    public imageUrl = this.publicService.urls.imageUrl;
    public defaultImage = "";

    constructor(public router: Router, public publicService: PublicService) { }

    changeUrl() {
        if (this.postType == 0) {
            let url = 'union/' + this.data.id;
            this.router.navigate([url]);
        }
        else if (this.postType == 1) {
            let url = 'professor/' + this.data.id;
            this.router.navigate([url]);

        }
        else if (this.postType == 2) {
            let url = 'startup/' + this.data.id;
            this.router.navigate([url]);

        }
    }
}