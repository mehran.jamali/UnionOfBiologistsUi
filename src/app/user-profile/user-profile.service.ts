import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';

import { DataService } from "../shared/services/data.service";
import { PublicService } from "../shared/services/public.service";

@Injectable()
export class UserProfileService {
    constructor(public dataService: DataService, public publicService: PublicService) { }

    // get user full info 
    getUserFullInfo(id): Observable<any> {
        let url = `api/UserProfile/GetUserFullInfo/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get user info
    getUserInfo(id): Observable<any> {
        let url = `api/UserProfile/GetUserInfo/${id}`
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get followed unions
    getFollowedUnions(data): Observable<any> {
        let url = 'api/UserProfile/GetFollowedUnions';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get followed startups
    getFollowedStartups(data): Observable<any> {
        let url = 'api/UserProfile/GetFollowedStartUps';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get followed professors
    getFollowedProfessors(data): Observable<any> {
        let url = 'api/UserProfile/GetFollowedProfessors';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response;
        }))
    }

}