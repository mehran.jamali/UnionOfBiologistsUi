import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile.component'
import { SharedModule } from '../shared/shared.module';
import { ProfileHomeComponent } from './profile-home/profile-home.component';
import { ProfileFollowedUnionComponent } from './profile-followed-union/profile-followed-union.component';
import { ProfileFollowedProfessorComponent } from './profile-followed-professor/profile-followed-professor.component';
import { ProfileFollowedStartupComponent } from './profile-followed-startup/profile-followed-startup.component';
import { ProfileFollowedItemComponent } from "./profile-followed-item/profile-followed-item.component";
import { UserProfileService } from "./user-profile.service";

@NgModule({
  declarations: [
    UserProfileComponent,
    ProfileHomeComponent,
    ProfileFollowedUnionComponent,
    ProfileFollowedProfessorComponent,
    ProfileFollowedStartupComponent,
    ProfileFollowedItemComponent
  ],
  imports: [
    SharedModule.forRoot()
  ],
  providers: [
    UserProfileService
  ]
})
export class UserProfileModule { }
