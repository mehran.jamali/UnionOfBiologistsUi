import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";
import { data } from 'jquery';

@Component({
  selector: 'app-profile-home',
  templateUrl: './profile-home.component.html',
  styleUrls: ['./profile-home.component.scss']
})
export class ProfileHomeComponent implements OnInit {

  // data from parent to chield
  @Input() data;

  // loader 
  public showLoader: boolean = true;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.showLoader = false;
    }, 500);
    AOS.init();
  }

}
