import { Component } from "@angular/core";
import * as $ from "jquery";
import { faAngleRight, faAngleLeft, faSearch } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import { FormControl, FormGroup } from "@angular/forms"

// Services
import { PublicService } from "../shared/services/public.service";
import { StartupService } from "./startup.service";

@Component({
    selector: 'app-startup',
    templateUrl: './startup.component.html',
    styleUrls: ['./startup.component.scss']
})

export class StartupComponent {
    // navbar
    public checkLogin: boolean = false;
    public navbarMode: boolean = false;

    // loader
    public showLoader: boolean = true;
    public showListLoader: boolean = false;

    // page
    public pageNumber = 1;
    public pageSize = 4;
    setDefaultPage() {
        this.pageNumber = 1;
        this.pageSize = 4;
    }


    // icons 
    faAngleRight = faAngleRight;
    faAngleLeft = faAngleLeft;
    faSearch = faSearch;

    // selected values
    public selectedValue = {
        "cityId": 0,
        "activity": 0,
    }

    // list
    public startups = []
    public provinces = []
    public cities = []
    public area = [
        { "id": 1, "name": "تولیدی" },
        { "id": 2, "name": "پژوهشی" },
        { "id": 3, "name": "آموزشی" },
    ]

    public filteredProvinces;
    public filteredCities;
    public filteredArea = this.area;

    // form
    public filterForm = new FormGroup({
        search: new FormControl("", []),
        province: new FormControl("", []),
        city: new FormControl({ value: '', disabled: true }, []),
        activity: new FormControl("", [])
    })


    constructor(public publicService: PublicService, public startupService: StartupService) { }

    // check token 
    checkToken() {
        this.publicService.checkToken().subscribe(data => {
            this.checkLogin = true;
        }, error => {
            this.checkLogin = false;
        })
    }

    // get porvince
    getProvinces() {
        this.publicService.getProvince().subscribe(data => {
            this.provinces = data;
            this.filteredProvinces = this.provinces;
        }, error => {
            // do nothing
        })
    }

    // get city by province id
    getCityByProvinceId(id) {
        this.publicService.getCityByProvinceId(id).subscribe(data => {
            this.cities = data;
            this.filteredCities = this.cities;
        }, error => {
            // do nothing
        })
    }

    goPrevious() {
        if (this.pageNumber > 1) {
            this.pageNumber -= 1;
            this.pageSize = 4;
            this.doFilter()
        }
        else {
            // do nothing
        }
    }

    // go next
    goNext() {
        if (this.startups.length < 4) {
            if (this.startups.length == 0) {
                this.pageNumber -= 1;
                this.pageSize = 4;
                this.doFilter()
            }
        }
        else {
            this.pageNumber += 1;
            this.pageSize = 4;
            this.doFilter()
        }
    }

    // get startup list with filter
    doFilter() {
        let body = {
            "cityId": this.selectedValue.cityId,
            "activity": this.selectedValue.activity,
            "searchText": this.filterForm.value.search ? this.filterForm.value.search : '',
            "pageSize": this.pageSize,
            "pageNumber": this.pageNumber,
        }
        this.startups = [];
        this.showListLoader = true;

        this.startupService.getStartUpList(body).subscribe(data => {
            this.startups = data;

            // list loader
            if (this.showLoader) {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 1500);
            }
            else {
                setTimeout(() => {
                    this.showListLoader = false;
                }, 500);
            }

            // loader
            setTimeout(() => {
                this.showLoader = false;
            }, 1000);
        }, error => {
            setTimeout(() => {
                this.showListLoader = false;
            }, 500);
        })
    }




    ngOnInit(): void {

        this.checkToken();
        this.doFilter();
        this.getProvinces();

        AOS.init();

        $(document).ready(function () {
            // open dropdown
            $(".select-input").focus(function () {
                $("#" + $(this).attr('name')).addClass("show-dropdown");
            })
            // close dropdown
            $(".select-input").focusout(function () {
                $("#" + $(this).attr('name')).removeClass("show-dropdown");
            })
        })

    }

    // select 
    // select province 
    setProvinceValue(value) {
        this.filterForm.controls.province.setValue(value.name);
        this.filterForm.controls['city'].enable();
        this.filteredProvinces = this.provinces;
        this.getCityByProvinceId(value.id);
        this.setDefaultPage()
    }

    // select city
    setCityValue(value) {
        this.filterForm.controls.city.setValue(value.name);
        this.selectedValue.cityId = value.id;
        this.filteredCities = this.cities;
        this.setDefaultPage()
    }

    // select activity
    setActivityValue(value) {
        this.filterForm.controls.activity.setValue(value.name);
        this.selectedValue.activity = value.id;
        this.filteredArea = this.area;
        this.setDefaultPage()
    }

    // filter 
    // filter province
    filterProvince() {
        this.filteredProvinces = this.provinces.filter(x => {
            return x.name.includes(this.filterForm.value.province);
        })
    }

    // filter city
    filterCity() {
        this.filteredCities = this.cities.filter(x => {
            return x.name.includes(this.filterForm.value.city);
        })
    }

    // filter activity
    filterActivity() {
        this.filteredArea = this.area.filter(x => {
            return x.name.includes(this.filterForm.value.activity);
        })
    }


}