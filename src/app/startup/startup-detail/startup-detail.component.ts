import { Component, OnInit } from '@angular/core';

import * as AOS from "aos";
import * as $ from "jquery";

import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

import { ActivatedRoute } from "@angular/router"

// service
import { StartupService } from "../startup.service";
import { PublicService } from "../../shared/services/public.service";
import { data } from 'jquery';

@Component({
  selector: 'app-startup-detail',
  templateUrl: './startup-detail.component.html',
  styleUrls: ['./startup-detail.component.scss']
})
export class StartupDetailComponent implements OnInit {

  // data for child components
  public isLogin: boolean = false;
  public navbarMode: boolean = true;

  // images
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultCoverImage = 'DefaultImages/startUpCover.jpg';
  public defaultProfileImage = 'DefaultImages/startUpProfile.jpg';

  // icons
  faAngleLeft = faAngleLeft;

  // tab name
  public tabName: string = '';

  // user id 
  public userId = '';

  // startUp detail
  public startUpDetail = {
    "name": "string",
    "profileImage": "string",
    "coverImage": "string",
    "slogan": "string"
  }

  // is follow 
  public isFollowed: boolean = false;

  // startup id
  public startUpId = '';

  // loader 
  public showLoader: boolean = true;
  public showContentLoader: boolean = false;

  constructor(public publicService: PublicService, public startupService: StartupService, public activatedRoute: ActivatedRoute) { }


  // check sidebar tabs
  changeSideTabs(value: string) {
    this.tabName = value;
    this.showContentLoader = true;
    setTimeout(() => {
      this.showContentLoader = false;
    }, 500);
  }


  // check token
  checkToken() {
    this.publicService.checkToken().subscribe(data => {
      this.isLogin = true;
    }, error => {
      this.isLogin = false;
    })
  }

  // get startup detail
  getStartUpDetail() {
    this.startupService.getStartUpDetail(this.startUpId).subscribe(data => {
      this.startUpDetail = data;
      setTimeout(() => {
        this.showLoader = false;
        this.showContentLoader = true;
        this.tabName = 'home'
        setTimeout(() => {
          this.showContentLoader = false;
        }, 500);
      }, 1000);
    }, error => { })
  }

  // check follow
  checkFollow() {
    this.startupService.checkStartUpFollow(this.startUpId).subscribe(data => {
      this.isFollowed = data;
      console.log(data);
    })
  }

  follow() {
    this.startupService.follow(this.startUpId).subscribe(data => {
      this.isFollowed = true;
    }, error => { })
  }

  unFollow() {
    this.startupService.unFollow(this.startUpId).subscribe(data => {
      this.isFollowed = false;
    }, error => { })
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('id');
    this.checkToken()

    this.startUpId = this.activatedRoute.snapshot.params['id']
    if (this.startUpId) {
      this.checkFollow()
      this.getStartUpDetail()
    }

    AOS.init();
    $(document).ready(function () {
      $('.tab').click(function () {
        $('.tab').removeClass('active');
        $(this).addClass('active');

        $('.content').removeClass('content-active');
        $('#' + $(this).attr('role')).addClass('content-active');
      })
    });
  }

}
