import { Component, OnInit } from '@angular/core';

import * as $ from "jquery";
import { ActivatedRoute } from "@angular/router";
import { PublicService } from "../../../shared/services/public.service";
import { StartupService } from "../../startup.service";


@Component({
  selector: 'app-detail-member',
  templateUrl: './detail-member.component.html',
  styleUrls: ['./detail-member.component.scss']
})
export class DetailMemberComponent implements OnInit {

  // loader
  public showContentLoader = false;

  // union id
  public unionId = ''

  // list
  public items: any = []

  constructor(public startupService: StartupService, public activatedRoute: ActivatedRoute, public publicService: PublicService) { }

  getFounders() {
    this.startupService.getFounders(this.unionId).subscribe(data => {
      this.items.push(...data);
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);

    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    this.unionId = this.activatedRoute.snapshot.params['id'];
    this.getFounders()
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }


  scroll = (event): void => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getFounders()
    }
  };

}
