import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import { faPhone, faMailBulk, faLocationArrow, faAddressCard } from "@fortawesome/free-solid-svg-icons"

import { ActivatedRoute } from "@angular/router";

// services
import { StartupService } from "../../startup.service";
import { PublicService } from "../../../shared/services/public.service";


@Component({
  selector: 'app-detail-contact-us',
  templateUrl: './detail-contact-us.component.html',
  styleUrls: ['./detail-contact-us.component.scss']
})
export class DetailContactUsComponent implements OnInit {
  // data
  public data = {
    "phoneNumber": "",
    "email": "",
    "telegram": "",
    "address": ""
  }

  // startUp id
  public startUpId = '';

  // loader
  public showContentLoader = false;

  // icons
  faPhone = faPhone;
  faMailBulk = faMailBulk;
  faLocationArrow = faLocationArrow;
  faSearchLocation = faAddressCard;

  constructor(public startupService: StartupService, public publicService: PublicService, public activatedRoute: ActivatedRoute) { }

  // get contact us
  getContactUs() {
    this.startupService.getContactUs(this.startUpId).subscribe(data => {
      this.data = data;
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => { })
  }

  ngOnInit() {
    this.startUpId = this.activatedRoute.snapshot.params['id']
    this.getContactUs()
    AOS.init()
  }

}
