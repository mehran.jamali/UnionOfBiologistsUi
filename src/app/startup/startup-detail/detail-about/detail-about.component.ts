import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";

import { ActivatedRoute } from "@angular/router";

// services
import { StartupService } from "../../startup.service";
import { PublicService } from "../../../shared/services/public.service";

@Component({
  selector: 'app-detail-about',
  templateUrl: './detail-about.component.html',
  styleUrls: ['./detail-about.component.scss']
})
export class DetailAboutComponent implements OnInit {

  // loader 
  public showContentLoader = false;

  // data
  public data: any = {
    'aboutStartUp': ''
  }

  // startup id
  public startUpId: string = '';

  constructor(public startupService: StartupService, public publicService: PublicService, public activatedRoute: ActivatedRoute) { }

  getAboutUs() {
    this.startupService.getAboutUs(this.startUpId).subscribe(data => {
      this.data = data;
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => { })
  }

  ngOnInit() {
    this.startUpId = this.activatedRoute.snapshot.params['id']
    this.getAboutUs();

    AOS.init();
  }

}
