import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";

// service
import { StartupService } from "../../../startup.service";
import { PublicService } from "../../../../shared/services/public.service";


@Component({
  selector: 'app-home-news',
  templateUrl: './home-news.component.html',
  styleUrls: ['./home-news.component.scss']
})
export class HomeNewsComponent implements OnInit {

  // loader
  public showContentLoader = false;

  // page
  public pageNumber = 0;
  public pageSize = 0;

  // startUp id
  public startUpId = ''

  // list
  public items: any = []
  constructor(public publicService: PublicService, public startupService: StartupService, public activatedRoute: ActivatedRoute) { }

  getArticles() {
    this.pageNumber += 1;
    this.pageSize = 3;
    let data = {
      "articleType": 4,
      "startUpId": this.startUpId,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }
    this.startupService.getArticles(data).subscribe(data => {
      this.items.push(...data);
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    this.startUpId = this.activatedRoute.snapshot.params['id'];
    this.getArticles()
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = () => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getArticles()
    }
  }

}

