import { Component, OnInit } from '@angular/core';
import * as AOS from "aos";
import * as $ from "jquery";


@Component({
  selector: 'app-detail-home',
  templateUrl: './detail-home.component.html',
  styleUrls: ['./detail-home.component.scss']
})
export class DetailHomeComponent implements OnInit {
  // title
  public contentTitle: string = "همه پست ها";

  // tab name
  public tabName: string = 'allPost';

  // loader 
  public showContentLoader: boolean = false;


  changeHomeTabs(value: string) {
    this.tabName = value;
    this.showContentLoader = true;
    setTimeout(() => {
      this.showContentLoader = false;
    }, 500);
    if (value == 'allPost') {
      this.contentTitle = "همه پست ها"
    }
    else if (value == 'article') {
      this.contentTitle = "مقالات"
    }
    else if (value == 'event') {
      this.contentTitle = "رویداد"
    }
    else if (value == 'news') {
      this.contentTitle = "اخبار"
    }
    else if (value == 'notification') {
      this.contentTitle = 'اطلاعیه'
    }
  }
  constructor() { }

  ngOnInit(): void {
    AOS.init()
    $(document).ready(function () {
      $('.nav-tab').click(function () {
        $('.nav-tab').removeClass('nav-active');
        $(this).addClass('nav-active');

        $('.nav-content').removeClass('nav-content-active');
        $('#' + $(this).attr('role')).addClass('nav-content-active');
      })

    });
  }

}
