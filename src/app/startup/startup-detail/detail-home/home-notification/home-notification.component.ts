import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";

// service
import { StartupService } from "../../../startup.service";
import { PublicService } from "../../../../shared/services/public.service";

@Component({
  selector: 'app-home-notification',
  templateUrl: './home-notification.component.html',
  styleUrls: ['./home-notification.component.scss']
})
export class HomeNotificationComponent implements OnInit {

  // loader
  public showContentLoader = false;

  // page
  public pageNumber = 0;
  public pageSize = 0;

  // startUp id
  public startUpId = ''

  // list
  public items: any = []
  constructor(public publicService: PublicService, public startupService: StartupService, public activatedRoute: ActivatedRoute) { }

  getNotifications() {
    this.pageNumber += 1;
    this.pageSize = 3;
    let data = {
      "startUpId": this.startUpId,
      "pageSize": this.pageSize,
      "pageNumber": this.pageNumber
    }
    this.startupService.getNotifications(data).subscribe(data => {
      this.items.push(...data);
      this.showContentLoader = true;
      setTimeout(() => {
        this.showContentLoader = false;
      }, 500);
    }, error => {
      console.clear()
    })
  }

  ngOnInit() {
    this.startUpId = this.activatedRoute.snapshot.params['id'];
    this.getNotifications()
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = () => {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      this.getNotifications()
    }
  }

}

