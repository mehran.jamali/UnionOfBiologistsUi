import { Component, OnInit, Input } from '@angular/core';
import * as AOS from "aos";
import {PublicService} from "../../shared/services/public.service";

@Component({
  selector: 'app-startup-item',
  templateUrl: './startup-item.component.html',
  styleUrls: ['./startup-item.component.scss']
})
export class StartupItemComponent implements OnInit {
  // data
  @Input() data: any;

  // image
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImageUrl = "DefaultImages/startup.jpg";

  constructor(public publicService: PublicService) { }

  ngOnInit() {
    AOS.init()
  }

}
