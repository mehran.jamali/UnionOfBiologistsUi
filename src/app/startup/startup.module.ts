// Modules
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";

// Service
import { StartupService } from "./startup.service";

// Components
import { StartupComponent } from "./startup.component";
import { StartupItemComponent } from './startup-item/startup-item.component';
import { StartupDetailComponent } from './startup-detail/startup-detail.component';
import { DetailAboutComponent } from './startup-detail/detail-about/detail-about.component';
import { DetailHomeComponent } from './startup-detail/detail-home/detail-home.component';
import { DetailContactUsComponent } from './startup-detail/detail-contact-us/detail-contact-us.component';
import { DetailEftekharatComponent } from './startup-detail/detail-eftekharat/detail-eftekharat.component';
import { DetailMemberComponent } from './startup-detail/detail-member/detail-member.component';
import { HomeAllpostComponent } from './startup-detail/detail-home/home-allpost/home-allpost.component';
import { HomeArticleComponent } from './startup-detail/detail-home/home-article/home-article.component';
import { HomeEventComponent } from './startup-detail/detail-home/home-event/home-event.component';
import { HomeNewsComponent } from './startup-detail/detail-home/home-news/home-news.component';
import { HomeNotificationComponent } from './startup-detail/detail-home/home-notification/home-notification.component';


@NgModule({
  declarations: [
    StartupComponent,
    StartupItemComponent,
    StartupDetailComponent,
    DetailAboutComponent,
    DetailHomeComponent,
    DetailContactUsComponent,
    DetailEftekharatComponent,
    DetailMemberComponent,
    HomeAllpostComponent,
    HomeArticleComponent,
    HomeEventComponent,
    HomeNewsComponent,
    HomeNotificationComponent,
  ],
  imports: [
    SharedModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    StartupService
  ]
})
export class StartupModule { }
