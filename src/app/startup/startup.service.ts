import { Injectable } from "@angular/core";
import { DataService } from "../shared/services/data.service";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';
import { ReplyComponent } from '../article-post/comment/reply/reply.component';

@Injectable()
export class StartupService {
    constructor(public dataService: DataService) { }

    // get startup list 
    getStartUpList(data): Observable<any> {
        let url = "api/List/GetStartUps";
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // get startup detail
    getStartUpDetail(id): Observable<any> {
        let url = `api/StartUpProfile/GetStartupInfo/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // check follow
    checkStartUpFollow(id): Observable<any> {
        let url = `api/StartUpProfile/IsFollowed/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // follow
    follow(id): Observable<any> {
        let url = `api/Follow/FollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // unfollow
    unFollow(id): Observable<any> {
        let url = `api/Follow/UnFollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get articles
    getArticles(data): Observable<any> {
        let url = 'api/StartUpProfile/GetArticles';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // get notification
    getNotifications(data): Observable<any> {
        let url = 'api/StartUpProfile/GetNotifications';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response
        }))
    }

    // get founders
    getFounders(id): Observable<any> {
        let url = `api/StartUpProfile/GetStartUpFounders/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // get honors
    getHonors(data): Observable<any> {
        let url = 'api/StartUpProfile/GetHonors';
        return this.dataService.post(url, data).pipe(map((response) => {
            return response;
        }))
    }

    // get aboutus
    getAboutUs(id): Observable<any> {
        let url = `api/StartUpProfile/GetStartUpAboutUsWeb/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response;
        }))
    }

    // get contact us
    getContactUs(id): Observable<any> {
        let url = `api/StartUpProfile/GetContactUs/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

}