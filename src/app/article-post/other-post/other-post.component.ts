import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { faHeart, faComment, faCopy, faPlus, faTimes, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import * as $ from "jquery";
import * as AOS from "aos";
import { PublicService } from "../../shared/services/public.service";
import { ArticlePostService } from "../article-post.service";

@Component({
  selector: 'app-other-post',
  templateUrl: './other-post.component.html',
  styleUrls: ['./other-post.component.scss']
})
export class OtherPostComponent implements OnInit {

  // navbar data
  public checkLogin: boolean;
  public navbarMode: boolean = true;

  // check
  public isFollowed: boolean;

  // id 
  public articleId: any;
  public userId: any;

  // user role
  public userRole: any;

  // file
  public imageUrl = this.publicService.urls.imageUrl
  public videoUrl = this.publicService.urls.videoUrl
  public voiceUrl = this.publicService.urls.voiceUrl
  public userDefaultImage = ''

  // loader 
  public showLoader: boolean = true;

  // data
  public data = {
    "id": "",
    "title": "",
    "text": "",
    "image": "",
    "video": "",
    "voice": "",
    "userId": "",
    "userFullName": "",
    "userProfileImage": "",
    "userField": "",
    "userRole": 1,
    "date": "",
    "articleType": "",
    "group": "",
    "visitNumber": 0,
    "likeNumber": 0,
    "commentNumber": 0
  };

  public comments = []

  // icons
  faHeart = faHeart;
  faComment = faComment;
  faCopy = faCopy;
  faPlus = faPlus;
  faTimes = faTimes;
  faArrowLeft = faArrowLeft;

  // form
  public commentForm = new FormGroup({
    text: new FormControl("", [Validators.required])
  });

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public publicService: PublicService, public articlePostService: ArticlePostService) { }

  // Check Token
  checkToken() {
    this.publicService.checkToken().subscribe(data => {
      this.checkLogin = true;
    }, error => {
      this.checkLogin = false;
    })
  }

  // Get Article
  getArticle() {
    this.articlePostService.getArticle(this.articleId).subscribe(data => {
      this.data = data;
      this.userRole = this.data.userRole;
      this.userId = this.data.userId;
      setTimeout(() => {
        this.showLoader = false;
      }, 1000);
      setTimeout(() => {
        this.checkFollow();
        this.getComments();
      }, 200);
    }, error => {
      console.log(error);
    })
  }

  // Check Follow
  checkFollow() {
    this.isFollowed = false;
    if (this.userRole == 1) {
      console.log('union check follow');
      this.articlePostService.checkUnionFollow(this.userId).subscribe(data => {
        if (data) {
          this.isFollowed = true;
        }
        else {
          this.isFollowed = false;
        }
      }, error => { })
    }
    else if (this.userRole == 2) {
      console.log('professor check follow');
      this.articlePostService.checkProfessorFollow(this.userId).subscribe(data => {
        if (data) {
          this.isFollowed = true;
        }
        else {
          this.isFollowed = false;
        }
      }, error => { })
    }
    else if (this.userRole == 3) {
      console.log('startup check follow');
      this.articlePostService.checkStartUpFollow(this.userId).subscribe(data => {
        if (data) {
          this.isFollowed = true;
        }
        else {
          this.isFollowed = false;
        }
      }, error => { })
    }
  }

  // Follow
  follow() {
    this.articlePostService.follow(this.userId).subscribe(data => {
      this.isFollowed = true;
    }, error => { })
    console.log("is follow");
  }

  // UnFollow
  unFollow() {
    this.articlePostService.unFollow(this.userId).subscribe(data => {
      this.isFollowed = false;
    }, error => { })
    console.log("unFollow");
  }

  // Like
  like() {
    let body = {
      "postId": this.articleId,
      "tableId": 1
    }
    this.articlePostService.like(body).subscribe(data => {
      this.data.likeNumber += 1;
      $('.like').addClass('liked');
    }, error => {
      $('.like').addClass('liked');
    })
  }

  // Get Comments
  getComments() {
    this.articlePostService.getComments(this.articleId).subscribe(data => {
      this.comments = data;
    }, error => {
      console.log('get comments error');
    })
  }

  // Send Comment
  sendComment() {
    let body = {
      "text": this.commentForm.value.text,
      "postId": this.articleId,
      "repliedCommentId": 0,
    }
    this.articlePostService.addComment(body).subscribe(data => {
      console.log(this.commentForm.value.text);
      this.getComments();
      this.commentForm.setValue({
        text: ''
      })
    }, error => { })
    // post id
  }

  // copy link
  copyLink() {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      let url = "localhost:4200" + this.router.url;
      e.clipboardData.setData('text/plain', url);
      e.preventDefault();
    });
    document.execCommand('copy');
  }


  ngOnInit(): void {

    this.checkToken()

    this.articleId = this.activatedRoute.snapshot.params['id'];
    if (this.articleId) {
      this.getArticle();
    }

    AOS.init();

    $(document).ready(function () {
      $('.tooltip').click(function () {
        $('.tooltip').removeClass('switch-tooltip');
        $(this).addClass('switch-tooltip');
        setTimeout(function () {
          $('.tooltip').removeClass('switch-tooltip');
        }, 1000)
      })
      $('#showComments').click(function () {
        $("#comment-box").slideToggle("2000");
      })
    })
  }

}
