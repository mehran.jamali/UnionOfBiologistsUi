import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';


import { DataService } from "../shared/services/data.service";
import { PublicService } from "../shared/services/public.service";
import { resolve } from 'url';

@Injectable()
export class ArticlePostService {
    constructor(public dataService: DataService, public publicService: PublicService) { }

    // Get Article
    getArticle(id: any): Observable<any> {
        let url = `api/Detail/GetArticle/${id}`;
        return this.dataService.get(url).pipe(map((response => {
            return response
        })))
    }

    // Get Notification
    getNotification(id: any): Observable<any> {
        let url = `api/Detail/GetNotification/${id}`;
        return this.dataService.get(url).pipe(map((response => {
            return response
        })))
    }

    // Check Follow
    checkProfessorFollow(id: any): Observable<any> {
        let url = `api/ProfessorProfile/IsFollowed/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    checkStartUpFollow(id: any): Observable<any> {
        let url = `api/StartUpProfile/IsFollowed/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    checkUnionFollow(id: any): Observable<any> {
        let url = `api/UnionProfile/IsFollowed/${id}`;
        return this.dataService.get(url).pipe(map((response) => {
            return response
        }))
    }

    // follow
    follow(id): Observable<any> {
        let url = `api/Follow/FollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // unfollow
    unFollow(id): Observable<any> {
        let url = `api/Follow/UnFollowUser/${id}`;
        return this.dataService.post(url).pipe(map((response: any) => {
            return response
        }))
    }

    // like 
    like(data: any): Observable<any> {
        let url = 'api/Like/Like';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // get comments
    getComments(id: any): Observable<any> {
        let url = `api/Detail/GetMainComments/${id}`;
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // get replied commment
    getRepliedComment(id: any): Observable<any> {
        let url = `api/Detail/GetRepliedComments/${id}`;
        return this.dataService.get(url).pipe(map((response: any) => {
            return response
        }))
    }

    // add comment
    addComment(data: any): Observable<any> {
        let url = 'api/Comment/AddComment';
        return this.dataService.post(url, data).pipe(map((response: any) => {
            return response
        }))
    }

    // delete comment
    deleteComment(id: any): Observable<any> {
        let url = `api/Comment/DeleteComment/${id}`;
        return this.dataService.delete(url).pipe(map((response: any) => {
            return response
        }))
    }


}