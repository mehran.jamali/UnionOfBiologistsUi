import { Component, Input } from "@angular/core"

@Component({
    selector: 'app-plyr',
    templateUrl: './plyr.component.html',
    styleUrls: ['./plyr.component.scss']
})

export class PlyrMediaComponent {
    @Input() src: string;
    @Input() poster: string;

    // or get it from plyrInit event
    // public player;


    ngOnInit() {
        // this.player = new Plyr('#plyrID', { captions: { active: true } });
    }

    constructor() { }
}