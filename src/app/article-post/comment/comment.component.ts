import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { faReply, faTrash, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery';
import * as AOS from "aos";
import { PublicService } from "../../shared/services/public.service";
import { ArticlePostService } from "../article-post.service";

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  // input
  @Input() data: any;
  @Input() postId: any;
  public userId;
  public activeDelete: boolean;

  public showComment = true;

  // image 
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImage = '';

  //data 
  public replies = []

  // icon
  faReply = faReply;
  faTrash = faTrash;
  faArrowLeft = faArrowLeft;

  // form
  public replyForm = new FormGroup({
    text: new FormControl("", [Validators.required])
  })

  // get reply
  getReplies() {
    this.articlePostService.getRepliedComment(this.data.id).subscribe(data => {
      this.replies = data;
    }, error => {
      console.log('you have an error in get replies !');
    })
  }

  // send reply
  sendReply() {
    console.log('send');
    let body = {
      "text": this.replyForm.value.text,
      "postId": this.postId,
      "repliedCommentId": this.data.id,
    }
    this.articlePostService.addComment(body).subscribe(data => {
      this.getReplies();
      this.replyForm.setValue({
        text: ''
      })
    }, error => { })
  }

  // show reply
  showReplies() {
    $(".reply-box").slideUp();
    $("#" + this.data.id).slideDown();
  }

  // delete comment
  deleteComment() {
    this.articlePostService.deleteComment(this.data.id).subscribe(data => {
      this.showComment = false;
    }, error => { })
  }


  constructor(public publicService: PublicService, public articlePostService: ArticlePostService) { }

  ngOnInit() {
    this.userId = localStorage.getItem('id');
    this.getReplies();
    setTimeout(() => {
      if (this.userId == this.data.userId) {
        this.activeDelete = true;
      }
      else {
        this.activeDelete = false;
      }
    }, 500);
    AOS.init();
    $(document).ready(function () { })

    if (this.userId == this.data.userId) {
      this.activeDelete = true;
    }
    else {
      this.activeDelete = false;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.userId == this.data.userId) {
      this.activeDelete = true;
    }
    else {
      this.activeDelete = false;
    }
  }

}
