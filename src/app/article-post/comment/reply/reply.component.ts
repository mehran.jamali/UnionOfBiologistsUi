import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { faTrash, faReply, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import * as AOS from "aos";
import * as $ from "jquery";
import { PublicService } from "../../../shared/services/public.service";
import { ArticlePostService } from "../../article-post.service";

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.scss']
})
export class ReplyComponent implements OnInit {

  @Input() data: any;
  @Input() postId: any;
  @Input() parentId: any;

  @Output() doGetReplied: EventEmitter<any> = new EventEmitter();

  public userId;
  public showComment = true;

  // image 
  public imageUrl = this.publicService.urls.imageUrl;
  public defaultImage = '';


  // icons
  faTrash = faTrash;
  faReply = faReply;
  faArrowLeft = faArrowLeft;
  // check
  public activeDelete = false;
  public replyId = null;

  public rReplyForm = new FormGroup({
    rText: new FormControl("", [Validators.required])
  })

  // send reply
  sendReply() {
    let body = {
      "text": this.rReplyForm.value.rText,
      "postId": this.postId,
      "repliedCommentId": this.parentId,
    }
    this.articlePostService.addComment(body).subscribe(data => {
      this.getReplied()
    }, error => { })
  }

  constructor(public publicService: PublicService, public articlePostService: ArticlePostService) { }

  // delete comment
  deleteComment() {
    this.articlePostService.deleteComment(this.data.id).subscribe(data => {
      this.showComment = false;
    }, error => { })
  }

  // get replied
  getReplied() {
    this.doGetReplied.emit();
  }

  ngOnInit() {

    this.userId = localStorage.getItem('id');
    setTimeout(() => {
      if (this.userId == this.data.userId) {
        this.activeDelete = true;
      }
      else {
        this.activeDelete = false;
      }
    }, 500);

    AOS.init();
    $(document).ready(function () { })

    if (this.userId == this.data.userId) {
      this.activeDelete = true;
    }
    else {
      this.activeDelete = false;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.userId == this.data.userId) {
      this.activeDelete = true;
    }
    else {
      this.activeDelete = false;
    }
  }


}
