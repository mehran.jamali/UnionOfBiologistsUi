// Modules
import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { PlyrModule } from "ngx-plyr";

// Components
import { NotificationPostComponent } from './notification-post/notification-post.component';
import { OtherPostComponent } from './other-post/other-post.component';
import { CommentComponent } from './comment/comment.component';
import { ReplyComponent } from './comment/reply/reply.component';
import { PlyrMediaComponent } from "./plyr/plyr.component";
import { MatVideoModule } from 'mat-video';

// Services
import { ArticlePostService } from "./article-post.service";

@NgModule({
  declarations: [NotificationPostComponent, OtherPostComponent, PlyrMediaComponent, CommentComponent, ReplyComponent],
  imports: [
    SharedModule.forRoot(),
    PlyrModule,
    MatVideoModule
  ],
  providers: [
    ArticlePostService
  ]
})
export class ArticlePostModule { }
